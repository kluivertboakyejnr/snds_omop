{{ config(materialized='incremental',
          unique_key='location_id') }}

-- Table LOCATION

WITH 
    union_locations AS ( 
        {{ dbt_utils.union_relations(
            relations=[
                ref('person_location__selected'),
                ref('dcir_location'),
                ref('pmsi_location') ]
            ) }}),
            
            
    create_location_id AS (
        SELECT DISTINCT
            {{ set_id(['location_source_value']) }}::bigint AS location_id,
            address_1::varchar(50)             AS address_1,
            address_2::varchar(50)             AS address_2,
            city::varchar(50)                  AS city,
            state::varchar(2)                  AS state,
            zip::varchar(50)                   AS zip,
            county::varchar(50)                AS county,
            location_source_value::varchar(50) AS location_source_value

        FROM union_locations
        
        )
        
        
SELECT *
FROM create_location_id
