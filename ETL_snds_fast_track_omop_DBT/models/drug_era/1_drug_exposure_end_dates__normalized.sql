{{ config(materialized='table') }}

WITH
drug_exposure_standard_concepts AS
(
    SELECT 
        d.drug_exposure_id,
        d.person_id, 
        d.drug_exposure_start_date AS drug_exposure_start_date, 
        d.days_supply AS days_supply,
        d.drug_concept_id, 
        COALESCE(
            NULLIF(drug_exposure_end_date, NULL), ---If drug_exposure_end_date != NULL, return drug_exposure_end_date, otherwise go to next case
            NULLIF(drug_exposure_start_date + days_supply::integer, drug_exposure_start_date), ---If days_supply != NULL or 0, return drug_exposure_start_date + days_supply, otherwise go to next case
            drug_exposure_start_date + 1 --Add 1 day to the drug_exposure_start_date since there is no end_date or INTERVAL for the days_supply
        ) AS drug_exposure_end_date
    FROM {{ref('drug_exposure')}} d
    WHERE drug_concept_id != 0 ---Our unmapped drug_concept_id's are set to 0, so we don't want different drugs wrapped up in the same era
),

ctePreDrugTarget(drug_exposure_id, person_id, ingredient_concept_id, drug_exposure_start_date, days_supply, drug_exposure_end_date) AS
(-- Normalize DRUG_EXPOSURE_END_DATE to either the existing drug exposure end date, or add days supply, or add 1 day to the start date
    SELECT
        d.drug_exposure_id, 
        d.person_id,
        ca.ingredient_concept_id, 
        d.drug_exposure_start_date, 
        d.days_supply AS days_supply, 
        d.drug_exposure_end_date
    FROM drug_exposure_standard_concepts d
    JOIN {{ref('0_ingredients_concept_ancestor_concept__joined')}} ca ON ca.descendant_concept_id = d.drug_concept_id
)

SELECT * FROM ctePreDrugTarget