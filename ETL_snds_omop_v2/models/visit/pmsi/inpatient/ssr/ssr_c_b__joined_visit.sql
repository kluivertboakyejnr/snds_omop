{{ config(materialized='table') }}


SELECT DISTINCT

    num_enq,
    exe_soi_dtd,
    exe_soi_dtf,
    base,
    finess_j,
    visit_nb,
    visit_source_value,
    admitting_source_value,
    discharge_to_source_value,
    provider_source_value,
    visit_concept_id
    
FROM {{ ref('ssr_c_b__joined') }}