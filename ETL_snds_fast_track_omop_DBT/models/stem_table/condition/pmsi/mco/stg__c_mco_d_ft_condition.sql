{{ config(materialized='view') }}

-- Ce modèle sélectionne les diagnostics associés dans les tables D du MCO FT

WITH diag_c_mco_ft_d_visit AS
(
    {{
        condition_source_values(
        src_table = 'snds.c_mco_d_ft', 
        source_values = ['ass_dgn'], 
        src_base = "'mcoft'", 
        src_finess_nb = 'eta_num', 
        src_visit_nb = 'rsa_num' 
    )
    
    }}
)

SELECT * FROM diag_c_mco_ft_d_visit