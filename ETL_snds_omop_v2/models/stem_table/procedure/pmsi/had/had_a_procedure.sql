{{ config(materialized='view') }}


WITH procedure_t_hadaaa AS (

    {% for year in var('years_pmsi') %}

        {% set source_name = 'HAD' %}
        {% set table_name = 't_had' ~ year ~ 'a'%}
        {% set base = ("'" ~ 'had' ~ year ~ "'") | string %}
    
        {% if year|int < 20 %}
    
        {% set delai = 'del_deb_sseq' %}
    
        {% else %}
    
        {% set delai = 'del_dat_ent' %}
    
        {% endif %}


        {{  procedure_pmsi_source_value(
            src_source = source_name,
            src_table = table_name, 
            source_value = 'ccam_cod', 
            src_base = base, 
            src_finess_nb = 'eta_num_epmsi', 
            src_visit_nb = 'rhad_num',
            src_sej_idt = 'rhad_num',
            phase = 'pha_cod',
            delai = delai  ) }}
    
        


        {% if not loop.last -%}
            UNION ALL 
            {%- endif -%}

    {%- endfor -%}   
)

SELECT * FROM procedure_t_hadaaa