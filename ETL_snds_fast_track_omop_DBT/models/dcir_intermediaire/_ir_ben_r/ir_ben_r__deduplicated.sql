{{config(materialized = 'table', indexes=[
      {'columns': ['num_enq'], 'type': 'hash'} ] )}}

-- Model de transformation de la table IR_BEN_R :
-- * récupération code commune
-- * récupération date de décès 
-- Lorsqu'une personne a ses données mises à jours (déménagement, décès...), une nouvelle ligne est créée avec la date de mise à jour. 
-- On supprime donc les informations obsolètes.  


WITH  keep_last_location AS 
(
    SELECT 
        DISTINCT 
        num_enq::varchar(100), 
        ben_sex_cod::varchar(50), 
        ben_nai_ann::integer, 
        ben_nai_moi::integer, 
        last_value(code_commune) OVER (
            PARTITION BY num_enq, ben_sex_cod, ben_nai_ann, ben_nai_moi
            ORDER BY ben_dte_maj 
            ROWS BETWEEN unbounded preceding and unbounded following 
        ) AS code_commune, -- on garde la dernière localisation connue 
        last_value(ben_dcd_dte) OVER (
            PARTITION BY num_enq, ben_sex_cod, ben_nai_ann, ben_nai_moi
            ORDER BY ben_dte_maj 
            ROWS BETWEEN unbounded preceding and unbounded following 
        ) AS ben_dcd_dte -- on récupère la date de décès s'il a eu lieu 
    FROM {{ref('code_commune__built')}}
)

SELECT * FROM keep_last_location 