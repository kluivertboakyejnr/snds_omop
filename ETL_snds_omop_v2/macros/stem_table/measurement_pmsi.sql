{% macro measurement_pmsi_source_value(src_source, src_table, source_value, src_base, src_finess_nb, src_visit_nb) %}


SELECT
    {{ source_value }}  AS source_value, 
    
    {{ visit_occurrence_source_value(src_base     = src_base,
                                    src_finess_nb = src_finess_nb,
                                    src_visit_nb  = src_visit_nb) }} AS visit_occurrence_source_value    
    
FROM {{ source(src_source, src_table) }}

{% endmacro %}