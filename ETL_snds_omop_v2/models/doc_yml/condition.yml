version: 2

models:

### HAD ###
  - name: had_b_condition
    description: Ce modèle sélectionne les diagnostics principaux présents dans les tables t_hadaaB

  - name: had_d_condition
    description: Ce modèle sélectionne les diagnostics associés présents dans les tables t_hadaaD

### MCO ###
  - name: mco_b_condition
    description: Ce modèle sélectionne les diagnostics principaux et reliés présents dans les tables t_mcoaaB ainsi que les variables nécessaires pour construire la variable visit_occurrence_source_value qui référence la visite pendant laquelle le diagnostic a été posé

  - name: mco_d_condition
    description: Ce modèle sélectionne les diagnostics associés présents dans les tables t_mcoaaD ainsi que les variables nécessaires pour construire la variable visit_occurrence_source_value qui référence la visite pendant laquelle le diagnostic a été posé

  - name: mco_um_condition
    description: Ce modèle sélectionne les diagnostics principaux et reliés présents dans les tables t_mcoaaUM ainsi que les variables nécessaires pour construire la variable visit_occurrence_source_value qui référence la visite pendant laquelle le diagnostic a été posé

### RIP ###
  - name: rip_rsa_c__joined__condition
    description: Ce modèle sélectionne les diagnostics principaux des tables t_ripaaRSA ainsi que les variables nécessaires pour construire la variable visit_occurrence_source_value qui référence la visite pendant laquelle le diagnostic a été posé. Il récupère aussi la date de début de visite dans la table C. 

### SSR ###
  - name: ssr_b_c__joined_condition
    description: Ce modèle sélectionne les diagnostics principaux des tables t_ssraaB ainsi que les variables nécessaires pour construire la variable visit_occurrence_source_value qui référence la visite pendant laquelle le diagnostic a été posé. Il récupère aussi la date de début de visite dans la table C. 

  - name : ssr_d_c__joined_condition
    description: Ce modèle sélectionne les diagnostics associés des tables t_ssraaD ainsi que les variables nécessaires pour construire la variable visit_occurrence_source_value qui référence la visite pendant laquelle le diagnostic a été posé. Il récupère aussi la date de début de visite dans la table C. 

### IR_IMB_R ###
  - name: ir_imb_r_fk_omop__joined
    description: Ce modèle sélectionne les diagnostics d'ALD provenant de la table IR_IMB_R. Il les joint ensuite à la table PERSON pour obtenir le person_id des personnes concernées, à la table DEATH pour indiquer la date de décès comme fin de l'ALD si le patient est décédé, et au modèle athena_mapping pour obtenir le source_concept_id et le concept_id correspondants au diagnostic du SNDS.
    columns: 
      - name: person_id
        description: Identifiant unique d'un patient 
      - name: visit_occurrence_id
        description: Identifiant unique d'une visite
      - name: provider_id
        description: Identifiant unique d'un professionnel de santé
      - name: start_date
        description: Date de début de l'ALD
      - name: end_date
        description: Date de fin de l'ALD si renseignée, sinon date de décès du patient
      - name: status_concept_id
        description: Type de diagnostic
      - name: source_value
        description: Code du diagnostic dans le SNDS
      - name: source_concept_id
        description: Concept associé au code source
      - name: concept_id
        description: Concept standard du diagnostic
      - name: domain_id
        description: Domaine du concept standard
      - name: visit_occurrence_source_value
        description: Identifiant d'une visite 

### Commun ###
  - name: diagnostic_associe__unioned
    description: Ce modèle unit les diagnostics associés provenant de toutes les tables D des champs MCO, HAD, SSR. Il les joint ensuite à la table VISIT_OCCURENCE pour récupérer des informations concernant la visite pendant laquelle un diagnostic a été posé (visit_occurrence_id, person_id, dates de la visite, provider)


  - name: diagnostic_principal__unioned
    description: Ce modèle unit tous les diagnostics principaux contenus dans les tables t_mcoaaB, c_mco_b_ft, t_mcoaaUM, c_mco_um_ft, t_hadaaB, t_ripaaRSA, t_ssraaB. Il les joint ensuite à la table VISIT_OCCURENCE pour récupérer des informations concernant la visite pendant laquelle un diagnostic a été posé (visit_occurrence_id, person_id, dates de la visite, provider)


  - name: diagnostic_relie__unioned
    description: Ce modèle unit tous les diagnostics reliés contenus dans les tables t_mcoaaB et t_mcoaaUM.Il les joint ensuite à la table VISIT_OCCURENCE pour récupérer des informations concernant la visite pendant laquelle un diagnostic a été posé (visit_occurrence_id, person_id, dates de la visite, provider)


  - name: condition_occurrence_diag__unioned
    description: Ce modèle unit les diagnostics principaux, reliés et associés et il sélectionne les variables pertinentes pour la STEM_TABLE
    columns:
      - name: person_id
        description: Identifiant d'une personne
        tests:
          - relationships:
              to: ref('person')
              field: person_id

      - name: visit_occurrence_id
        description: Identifiant de la visite lors de laquelle a été posé le diagnostic
        tests:
          - relationships:
              to: ref('visit_occurrence')
              field: visit_occurrence_id

      - name: provider_id
        description: Identifiant du praticien ayant posé le diagnostic
        tests:
          - relationships:
             to: ref('provider')
             field: provider_id

      - name: concept_id
        description: Dans OMOP, concept standard décrivant le diagnostic posé
        tests:
          - not_null  
          - relationships:
              to: source('vocabularies','concept')
              field: concept_id

      - name: source_value
        description: Dans le SNDS, code qui décrit le diagnostic

      - name: source_concept_id
        description: Concept associé au code source du SNDS décrivant le dignostic
        tests:
          - relationships:
              to: source('vocabularies','concept')

      - name: type_concept_id
        description: Type d'enregistrement dans la base de données
        tests:
          - not_null
          - relationships:
              to: source('vocabularies','concept')
              field: concept_id

      - name: start_date
        description: Date de début de la visite lors de laquelle a été posé le disagnostic
        tests:
          - not_null

      - name: end_date
        description: Date de fin de la visite lors de laquelle a été posé le diagnostic

      - name: domain_id
        description: Domain auquel appartient le concept standard OMOP décrivant le diagnostic 

      - name: status_concept_id
        description: Concept standard définissant le type de diagnostic, primaire ou secondaire.
        tests:
          - relationships:
              to: source('vocabularies','concept')
              field: concept_id

      - name: status_source_value
        description: Dans le SNDS, définit le type de diagnostic, principal, relié ou associé.