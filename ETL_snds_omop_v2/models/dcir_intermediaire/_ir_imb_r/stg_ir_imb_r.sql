{{config(materialized = 'view')}}
WITH 
_ir_imb_r AS 
(
    SELECT
        TRIM(med_mtf_cod)::varchar(20)              AS med_mtf_cod,
        {{format_date('imb_ald_dtd')}}              AS imb_ald_dtd,                         
        {{format_date('imb_ald_dtf')}}              AS imb_ald_dtf,
        {{format_date('ins_dte')}}                  AS ins_dte,
        {{format_date('upd_dte')}}                  AS upd_dte,
        imb_etm_nat::varchar(50),
        num_enq::varchar(100)                  
    FROM
        {{source('beneficiaire','ir_imb_r')}}
)

SELECT * FROM _ir_imb_r