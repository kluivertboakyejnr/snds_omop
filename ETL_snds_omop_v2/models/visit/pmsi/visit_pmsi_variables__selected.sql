{{ config(materialized='view') }}
        
        
WITH
    tables_pmsi__unioned AS(
        {{ dbt_utils.union_relations(
            relations=[
                ref('had_c_b__joined'),
                ref('ssr_c_b__joined_visit'),
                ref('mco_c_b__joined'),
                ref('rip_c_rsa__joined'),
                
                ref('mco_cstc_fbstc__joined'),
                ref('ssr_cstc_fbstc__joined')
            ] ) }} ),
            
            
            
    variables_for_visit__selected AS(        
        SELECT DISTINCT
            num_enq                                             AS person_source_value,
            visit_concept_id,
            exe_soi_dtd                                         AS visit_start_date,
            exe_soi_dtf                                         AS visit_end_date,
            finess_j                                            AS care_site_source_value,
        
           {{ visit_occurrence_source_value(src_base       = 'base',
                                            src_finess_nb  = 'finess_j',
                                            src_visit_nb   = 'visit_nb',
                                            src_start_date = 'exe_soi_dtd') }} AS visit_occurrence_source_value,
        
            visit_source_value,
        
                           
            admitting_source_value,
            discharge_to_source_value,
            base,
            MAX(provider_source_value) AS provider_source_value


        FROM tables_pmsi__unioned
        WHERE exe_soi_dtd IS NOT NULL
        GROUP BY 1,2,3,4,5,6,7,8,9,10)
        
        
SELECT 
    

* FROM variables_for_visit__selected
    
    
    
    
