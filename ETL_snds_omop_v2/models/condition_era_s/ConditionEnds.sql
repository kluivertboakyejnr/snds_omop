{{ config(materialized='table') }}

WITH    
    condition_target_end_dates__joined AS(
        SELECT 
            c.person_id,
            c.condition_concept_id,
            c.condition_start_date,

            e.end_date
        
        FROM {{ ref('ConditionTarget') }} c

        INNER JOIN {{ ref('CondEndDates') }} e
            ON c.person_id             = e.person_id
            AND c.condition_concept_id = e.condition_concept_id
            AND e.end_date            >= c.condition_start_date),



    era_end_date__selected AS (
        SELECT 
            person_id,
            condition_concept_id,
            condition_start_date,
            MIN(end_date) AS era_end_date

        FROM condition_target_end_dates__joined

        GROUP BY 1,2,3)


SELECT * FROM era_end_date__selected