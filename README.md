# Standardisation du SNDS au format OMOP-CDM

Ce dépôt git contient les scripts SQL, sous forme de projet [DBT](https://www.getdbt.com/), qui ont permis d'effecuer la standardisation de la base principale du SNDS au format OMOP. Des erreurs peuvent subsister et les choix qui ont été posés peuvent être challengés, alors n'hésitez pas à contriber à leur amélioration en créant des [issues](https://gitlab.com/healthdatahub/snds_omop/-/issues) sur ce dépôt GitLab ! 

Deux versions de cette transformation existent : 
- V1 : [`ETL_snds_fast_track_omop_DBT`](./ETL_snds_fast_track_omop_DBT/) concerne les données du SNDS Fast-Track, c'est-à-dire les données SNDS de patients ayant reçu un diagnostic COVID-19 au cours d'une visite à l'hôpital. Elles comportent : 
  - les données du DCIR de mars 2019 à avril 2020 
  - les données du PMSI non consolidées de 2019 (tables 19_09) 
  - les tables PMSI spécifiques au COVID, préfixées C_MCO
  - le référentiel des bénéficiaires
  - le référentiel médicalisé 
  - le référentiel EHPAD. 

- [NOUVEAU] V2 : [`ETL_snds_omop_v2`](./ETL_snds_omop_v2/) concerne les données d'une cohorte de 570 000 patients sur la période 2015-2021 . Elles comportent :
  - les données du DCIR de 2015 à 2021
  - les données du PMSI de 2015 à 2021
  - les données du CépiDc de 2015 à 2017
  - le référentiel des bénéficiaires
  - le référentiel médicalisé.

:warning: Les scripts disponibles sur ce dépôt gitlab sont adaptés à l'extraction sur laquelle les transformations ont été réalisées. En cas d'utilisation pour une autre extraction, il se peut que des adapations soient à faire en raison des spécifités du SNDS. Vous pouvez paramétrer les années d'étude dans le fichier `dbt_project.yml`.

## Outils utilisés pour la V2

- dbt       : version 1.5.0
- dbt-utils : version 1.1.0
- Postgres  : version 13.2

## Comment utiliser ce dépôt GIT ?
- Le dossier sources contient les fichiers de correspondances et les nomenclatures du SNDS nécessaires pour la standardisation.
- Le dossier ETL_snds_omop_v2 contient les scripts SQL sous forme de projet DBT

### Prérequis
- Données sources SNDS dans une base de données (Postgres dans nos travaux)
- Avoir [DBT installé](https://docs.getdbt.com/docs/get-started/installation)
- Nous vous conseillons fortement de créer la structure de la base de données au format OMOP, d'implémenter les contraintes de clés primaires et étrangères, et de construire des indices avant de lancer l'exécution des scripts. Pour ce faire, vous trouverez des scripts PostgreSQL sur le [dépôt git d'OHDSI](https://github.com/OHDSI/CommonDataModel/tree/main/inst/ddl/5.3/postgresql). 

### Initialisation du projet
Pour utiliser ce projet dbt, nous vous conseillons de:
- Cloner ce repo git en local
- Se placer dans le dossier `ETL_snds_omop_v2`
- Exécuter `dbt init` pour [initaliser le projet dbt](https://docs.getdbt.com/reference/commands/init#existing-project). 

- Configurer le fichier `profiles.yml` comme indiqué sur la [documentation DBT](https://docs.getdbt.com/docs/get-started/connection-profiles). Si vous avez exécuté DBT pour la première fois, DBT a normalement créé un fichier `profiles.yml`  dans un dossier caché `.dbt` en exécutant l'étape précédente.  
```ruby
# Exemple de profile dbt
mon_profile_dbt: # nom du profile 
  target: dev
  outputs:
    dev:
      type: postgres # Type de base de données 
      host: localhost
      user: <user> 
      password: <password>
      port: 5432 # Image postgres
      dbname: <database name>
      schema: snds #schéma contenant les données snds 
      threads: 5
```
- Configurer le fichier [`dbt_project.yml`](ETL_snds_omop_v2/dbt_project.yml) (notamment changer le nom du projet et le nom du profile)


### Utilisation du projet DBT
Si vous avez suivi les instructions de la partie "Initialisation du projet", vous devriez pouvoir à présent utiliser le projet DBT !

- Pour exécuter tous les modèles : `dbt run`
- Pour exécuter un modèle en particulier : `dbt run --select <nom_modèle>`
- Pour lancer les tests contre le modèle : `dbt test`
- Pour générer la documentation dbt : `dbt docs generate` puis `dbt docs serve --port <numero_port>`

## Licence
Tous les contenu partagés dans ce dépôt sont publiés sous [licence Apache](https://fr.wikipedia.org/wiki/Licence_Apache) version 2.0.
Une copie de la licence est disponible dans le fichier [LICENCE](LICENCE), ou à l'adresse http://www.apache.org/licenses/LICENSE-2.0.  

### Explications

La licence `Apache-2.0` est une [licence libre](https://fr.wikipedia.org/wiki/Licence_libre).
Cela signifie qu'elle donne à quiconque la liberté d'utiliser, d'étudier, de modifier et de redistribuer les contenus partagés dans ce dépôt, _à condition_ que la licence et les auteurs de ces contenus soient mentionnés.

### Remarque

Lorsqu'un contenu produit dans le cadre d'une mission de service public est partagé _sans licence_, les conditions de sa réutilisation sont fixées par le [Code des Relations entre le Public et l'Administration](https://www.legifrance.gouv.fr/affichCode.do?cidTexte=LEGITEXT000031366350).

Un guide juridique pour la publication de logiciels libres dans l'administration est disponible sur le site internet de la mission Etalab : [guide-juridique-logiciel-libre.etalab.gouv.fr/](https://guide-juridique-logiciel-libre.etalab.gouv.fr/).
