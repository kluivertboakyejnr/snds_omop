{% macro correct_code_commune(code) %}

-- Cette macro transforme le code des communes corses présentes dans la liste de l'INSEE en remplaçant les codes 2A et 2B par 20:
-- Dans la variable ben_res_dpt (numéro du département) du SNDS qui est utilisée pour calculer le code commune,
-- il n'y a pas de distinction entre la Haute-Corse (2B) et la Corse-du-Sud (2A) : les deux sont codées 209  
-- contrairement aux code communes de l'INSEE. 
-- On réalise cette transformation pour permettre les alignements. 




CASE
    WHEN SUBSTR({{ code }}, 1, 2) = '2A'
        THEN '20' || SUBSTR({{ code }}, 3,3)
    WHEN SUBSTR({{ code }}, 1, 2) = '2B'
        THEN '20' || SUBSTR({{ code }}, 3,3)
    ELSE
        {{ code }}
END

{% endmacro %}

