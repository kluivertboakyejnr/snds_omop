{{ config(materialized='view') }}

-- Finess juridiques et géographiques et exclue les finess juridiques en double

WITH 
    finess_geo__unioned AS(
        {{ dbt_utils.union_relations(
            relations=[
                    ref('stg__t_mcoaaum'),
                    ref('stg__t_hadaab'),
                    ref('stg__t_ssraab'),
                    ref('stg__t_ripaac')
              ] ) }} ),
            
    exclude_finess AS(
        SELECT *
        FROM finess_geo__unioned fgu
    
        WHERE NOT EXISTS(
                SELECT *
                FROM {{ source('finess', 'finess_exclusion_list') }} fel
                WHERE fel.finess_to_exclude = fgu.finess_j)) -- Exclusion des FINESS géographiques APHP/APHM/HCL pour éviter les doublons (jusqu’en 2018) 
            
            
SELECT * FROM exclude_finess
