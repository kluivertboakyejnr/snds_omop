
-- Ce modèle sélectionne les variables des tables t_mcoaaA utiles pour calculer visit_source_value associé à la visite pendant laquelle un actes a été réalisé, ainsi que celles qui alimenteront la table PROCEDURE_OCCURRENCE 

WITH procedure_t_mcoaaa AS
(
    {{
    procedure_pmsi_source_value(
        src_table = 'snds.t_mco19_09a', 
        source_value = 'cdc_act', 
        src_base = "'mco19_09'", 
        src_finess_nb = 'eta_num', 
        src_visit_nb = 'rsa_num',
        phase = 'pha_act',
        quantity = 'nbr_exe_act',
        delai = 'ent_dat_del' 
    )
}}
)

SELECT * FROM procedure_t_mcoaaa