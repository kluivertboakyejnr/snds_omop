{{ config(materialized='view') }}

-- Ce modèle sélectionne les numéros finess juridiques des établissements du MCO contenus dans les tables t_mcoaaE

SELECT DISTINCT
    eta_num::varchar(50) AS finess_j
    
FROM snds.t_mco19_09e

