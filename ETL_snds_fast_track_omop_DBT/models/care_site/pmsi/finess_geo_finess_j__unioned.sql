{{ config(materialized='view') }}

-- Ce model permet d'obtenir la liste des finess juridiques et géographiques ainsi que les raisons sociales correspondantes.

WITH 
    finess_geo AS (
        SELECT 
            finess_geo AS finess,
            rs         AS care_site_name
        FROM {{ ref('finess_geo__mapped') }} ), -- Les finess geographiques et les raisons correspondantes (Dossier LOCATION/PMSI)
        
        
    finess_j AS (
        SELECT
            finess_j AS finess,
            ej_rs    AS care_site_name
        FROM {{ ref('finess_j__mapped') }} ) -- Les finess juridiques et les raisons sociales correspondantes (DOSSIER CARE_SITE/PMSI)
        

SELECT * FROM finess_geo
UNION ALL
SELECT * FROM finess_j 
