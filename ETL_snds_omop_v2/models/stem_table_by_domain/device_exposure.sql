{{config(materialized='incremental', 
         schema='omop',
         unique_key = 'device_exposure_id',
         alias='device_exposure')}}
        

         
SELECT
    DISTINCT
    id::bigint                  AS device_exposure_id,
    person_id::bigint           AS person_id,
    concept_id::integer         AS device_concept_id,
    start_date::date            AS device_exposure_start_date,
    start_datetime::timestamp   AS device_exposure_start_datetime,
    end_date::date              AS device_exposure_end_date,
    end_datetime::timestamp     AS device_exposure_end_datetime,
    type_concept_id::integer    AS device_type_concept_id,
    unique_device_id::integer   AS unique_device_id,
    quantity::integer           AS quantity,
    provider_id::bigint         AS provider_id,
    visit_occurrence_id::bigint AS visit_occurrence_id,
    visit_detail_id::bigint     AS visit_detail_id,
    source_value::varchar(50)   AS device_source_value,
    source_concept_id::integer  AS device_source_concept_id
FROM 
{{ref('stem_table_omop')}}
WHERE domain_id='Device'

