{{ config(materialized='view') }}

WITH 
    procedure_t_mcoaaa AS (

        {% for year in var('years_pmsi') %}

            {% set source_name = 'MCO' %}
            {% set table_name = 't_mco' ~ year ~ 'a'%}

            {% set base = ("'" ~ 'mco' ~ year ~ "'") | string %}

            {{ procedure_pmsi_source_value(
                src_source = source_name,
                src_table = table_name, 
                source_value = 'cdc_act', 
                src_base = base, 
                src_finess_nb = 'eta_num', 
                src_visit_nb = 'rsa_num',
                src_sej_idt = 'rsa_num',
                phase = 'pha_act',
                quantity = 'nbr_exe_act',
                delai = 'ent_dat_del') }}

            {% if not loop.last -%}
            UNION ALL 
            {%- endif -%}

        {%- endfor -%}   )

SELECT * FROM procedure_t_mcoaaa