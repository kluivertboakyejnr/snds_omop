WITH 
dcir_procedure 
AS 
(
    SELECT 
    cam_prs_ide   AS source_value,
    quantity,
    {{
        visit_occurrence_source_value(
            src_base="'dcir'", 
            src_finess_nb='etb_pre_fin', 
            src_visit_nb='dcir_visit_id' 
        )
        }}           
        AS visit_occurrence_source_value
FROM 
    {{ref('ccam_quantity__grouped')}} 

)

SELECT * FROM dcir_procedure