{{ config(materialized='view') }}

-- Ce modèle sélectionne les diagnostics associés dans les tables t_hadaaD

WITH diag_t_hadaad_visit
AS
(
{{ condition_source_values(
        src_table = 'snds.t_had19_09d',
        source_values = ['dgn_ass'],
        src_base="'had19_09'",
        src_finess_nb='eta_num_epmsi',
        src_visit_nb='rhad_num'
)
}}
    )


SELECT * FROM diag_t_hadaad_visit