{{config(materialized='table', indexes=[
      {'columns': ['dcir_visit_id', 'tip_prs_ide', 'tip_prs_typ', 'quantity'], 'unique':True}
    ])}}
    
-- Ce modèle prend en compte les régularisations dans le SNDS au niveau des quantités de prestations et des dates.  

WITH 
fix_tip_quantity_dates
AS 
(
    SELECT 
        dcir_visit_id,
        tip_prs_ide, 
        pfs_exe_num,
        etb_pre_fin,
        exe_soi_dtd,
        tip_prs_typ,
    
        SUM(tip_act_qsn) AS quantity,
        MAX(tip_acl_dtd) AS tip_acl_dtd,  -- On ne prend pas en compte les valeurs nulles des lignes de régularisation
        MAX(tip_acl_dtf) AS tip_acl_dtf -- On ne prend pas en compte les valeurs nulles des lignes de régularisation
    
    FROM {{ref('tip_prs__joined')}}  
    GROUP BY 1,2,3,4,5,6
)

SELECT 
    DISTINCT 
    dcir_visit_id,
    tip_prs_ide, 
    pfs_exe_num,
    etb_pre_fin,
    tip_prs_typ,
    
    quantity ,
    COALESCE(tip_acl_dtd, exe_soi_dtd) AS tip_acl_dtd,  -- Lorsque la date d'achat ou de location n'est pas remplie, on prend la date de début des soins. 
    tip_acl_dtf 
FROM fix_tip_quantity_dates