{{ config(materialized='view') }}

-- Ce modèle joint les tables T_RIPaaC et T_RIPaaB

WITH

    join_c_rsa AS(
        SELECT
            c.num_enq,
            c.exe_soi_dtd,
            MAX(c.exe_soi_dtf) AS exe_soi_dtf,
            c.base AS base,
            c.finess_j,
        
            COALESCE(rsa.sej_idt, c.visit_nb)          AS visit_nb,
            NULL                 AS visit_source_value,
            MAX(rsa.ent_prv)     AS admitting_source_value,
            MAX(rsa.sor_des)     AS discharge_to_source_value,
        
            NULL            AS provider_source_value,
        
            9201 AS visit_concept_id

        FROM {{ ref('stg__t_ripaac_visit') }} c
        JOIN {{ ref('stg__t_ripaarsa') }} rsa using(finess_j, visit_nb, base)
        GROUP BY 1,2,4,5,6,7,10,11
        
            
    ),
    
    double_sejour AS( -- Séjours avec les mêmes identifiants 
        SELECT DISTINCT
            base, 
            finess_j, 
            visit_nb, 
            exe_soi_dtd, 
            COUNT(*)
        
        FROM join_c_rsa
        
        GROUP BY 1,2,3,4
        
        HAVING COUNT(*)>1),
        
    remove_double_sejours AS( -- On retire les séjours qui ont des identifiants en double, mais qui ne sont pas le même séjour (personnes différentes)
        SELECT joi.* 
        FROM join_c_rsa joi
        LEFT JOIN double_sejour ds using(base, finess_j, visit_nb, exe_soi_dtd)
        WHERE ds.base is null
        AND ds.finess_j is null
        AND ds.visit_nb is null
        AND exe_soi_dtd is null
        )
    

SELECT * FROM remove_double_sejours