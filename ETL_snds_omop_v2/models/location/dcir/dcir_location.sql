{{ config(materialized='view') }}

-- Ce modèle représente la partie de la table LOCATION concernant le DCIR
WITH 

    dcir_mapped AS(
        SELECT *
        FROM {{ ref('healthcare_facilities_pharmacies__mapped') }} ),
            
    dcir_location AS(
        SELECT DISTINCT
            dm.adresse                             AS address_1,
            NULL::varchar(50)                      AS address_2,
            dm.lib_acheminement                    AS city,
            dm.code_region                         AS state,
            dm.com_code                            AS zip,
            SUBSTR(dm.com_code, 1, 2)::varchar(20) AS county,
            dm.finess8                             AS location_source_value 
            
            
        FROM dcir_mapped dm)
            
            
SELECT * FROM dcir_location