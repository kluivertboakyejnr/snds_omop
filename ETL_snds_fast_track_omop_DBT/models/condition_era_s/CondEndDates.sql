{{ config(materialized='view') }}

WITH    
    start_day_number__given AS(
        --Select the start dates, assigning a row number to each
        SELECT  
            person_id,
            condition_concept_id,
            condition_start_date AS event_date,
            -1::integer AS event_type,

            ROW_NUMBER() OVER(
                PARTITION BY
                    person_id,
                    condition_concept_id
                ORDER BY condition_start_date) AS start_ordinal

        FROM {{ ref('ConditionTarget') }}),


    end_date__modified AS(
        SELECT 
            person_id,
            condition_concept_id,
            condition_end_date + INTERVAL '30 days',
            1::integer AS event_type,
            NULL::integer
        
        FROM {{ ref('ConditionTarget')}} ),


    raw_data AS(
        SELECT * FROM start_day_number__given
        UNION ALL 
        SELECT * FROM end_date__modified),

    E1 AS(
        SELECT  
            person_id,
            condition_concept_id,
            event_date,
            event_type,
            start_ordinal,
            ROW_NUMBER () OVER(
                PARTITION BY        
                    person_id,
                    condition_concept_id
                ORDER BY event_date, event_type) AS overall_ord


        FROM raw_data),
    

    E2 AS(
        --SELECT as start_number__given ? 

        SELECT
            person_id,
            condition_concept_id,
            condition_start_date AS event_date,
            ROW_NUMBER() OVER (
                PARTITION BY person_id, condition_concept_id
                ORDER BY condition_start_date) AS start_ordinal

        FROM {{ ref('ConditionTarget') }}),



    E AS(
        SELECT  
            E1.person_id,
            E1.condition_concept_id,
            E1.event_date,
            E1.start_ordinal AS start_ordinal1,
            E1.overall_ord,
            E2.start_ordinal AS start_ordinal2

        FROM E1 
        INNER JOIN E2
            ON E1.person_id             = E2.person_id
            AND E1.condition_concept_id = E2.condition_concept_id
            AND E2.event_date          <= E1.event_date),


    E_bis AS(
        SELECT
            person_id,
            condition_concept_id,
            event_date,
            COALESCE(start_ordinal1, MAX(start_ordinal2)) AS start_ordinal,
            overall_ord
        FROM E

        GROUP BY 
            person_id,
            condition_concept_id,
            event_date,
            start_ordinal1,
            overall_ord)

SELECT 
    person_id,
    condition_concept_id,
    event_date - INTERVAL '30 days' AS end_date -- unpad this date

FROM E_bis

WHERE (2 * start_ordinal) - overall_ord = 0 


