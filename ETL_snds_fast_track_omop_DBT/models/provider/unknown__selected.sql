{{ config(materialized='view', schema='provider') }}

-- Ce model crée le provider fictif correspondant au cas ou pse_spe_cod et pse_act_nat sont nuls.

SELECT DISTINCT
    NULL    AS speciality_source_value,
    '99_99' AS provider_source_value,
    NULL    AS speciality_concept_id
    
FROM snds.ir_spe_v


UNION ALL

SELECT DISTINCT
    NULL    AS speciality_source_value,
    '0_0' AS provider_source_value,
    NULL    AS speciality_concept_id
    
FROM snds.ir_spe_v

UNION ALL

SELECT DISTINCT
    NULL    AS speciality_source_value,
    '99_99' AS provider_source_value,
    NULL    AS speciality_concept_id
    
FROM snds.ir_spe_v