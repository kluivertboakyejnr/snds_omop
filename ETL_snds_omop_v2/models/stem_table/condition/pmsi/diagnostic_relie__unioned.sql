{% set condition_status = "'Diagnostic Relié'" %}
{% set condition_status_concept_id = 32908 %}

WITH 
    diag_rel AS (
        {{ condition_visit_status(
                src_table=ref('mco_b_condition'),
                src_value='dgn_rel',
                status = condition_status,
                status_concept_id = condition_status_concept_id) }}
        UNION ALL
        {{ condition_visit_status(
                src_table=ref('mco_um_condition'),
                src_value='dgn_rel',
                status = condition_status,
                status_concept_id = condition_status_concept_id) }} ),
                
                


    visits AS (
        SELECT 
            vo.person_id,
            vo.visit_occurrence_id,
            --vd.visit_detail_id,
            vo.provider_id,
            vo.visit_start_date,
            vo.visit_end_date,
            vo.visit_occurrence_source_value
        FROM 
            {{ref('visit_occurrence_pmsi')}} vo 
            --JOIN {{ref('visit_detail')}} vd USING(visit_occurrence_id) 
    ),


    join_visit_concept AS (
        SELECT
            DISTINCT
                person_id               AS person_id,
                visit_occurrence_id     AS visit_occurrence_id,
                provider_id             AS provider_id,
                visit_start_date        AS start_date,
                visit_end_date          AS end_date,
                status_concept_id       AS status_concept_id,
                status_source_value     AS status_source_value,
                source_value            AS source_value,
                concept_id_1            AS source_concept_id,
                concept_id_2            AS concept_id,
                --visit_detail_id         AS visit_detail_id,
                domain_id               AS domain_id,
                visit_occurrence_source_value

        FROM
                diag_rel
                JOIN visits USING(visit_occurrence_source_value)
                JOIN  {{ref('athena_mapping')}} ON source_value = REPLACE(concept_code, '.', '')

        )

SELECT * FROM join_visit_concept 
