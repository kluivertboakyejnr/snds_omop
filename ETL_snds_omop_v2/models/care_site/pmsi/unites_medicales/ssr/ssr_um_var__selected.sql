{{ config(materialized='view') }}

-- Ce modèle sélectionne et renomme les variables du modèle ssr_um__joined qui seront utilisées dans la table care_site

SELECT
    type_autorisation AS care_site_name,
    type_um           AS place_of_service_source_value,
    type_um           AS care_site_source_value
    
FROM {{ ref('ssr_um__joined') }}