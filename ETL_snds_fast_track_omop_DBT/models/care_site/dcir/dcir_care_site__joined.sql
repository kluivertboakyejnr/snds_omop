{{ config(materialized='view') }}

-- Ce modèle crée la partie de la table CARE_SITE correspondant au DCIR en joignant le modèle healthcare_facilities_pharmacies__mapped avec la table LOCATION. 

WITH 
    pharmacy_facilities__unioned AS(
        SELECT 
            finess8,
            rs
        FROM {{ ref('healthcare_facilities_pharmacies__mapped') }} ), -- Numéros finess8 des établissements de santé et des pharmacies, leurs adresses, et leurs raisons sociales.
                
                
    location AS(
        SELECT 
            location_id,
            location_source_value
        FROM {{ ref('location') }}),
                
                
    pharmacy_hospital_location AS(

        SELECT DISTINCT
                l.location_id::bigint                AS location_id,
                l.location_source_value::varchar(50) AS care_site_source_value,

                pf.rs::varchar(255)                  AS care_site_name,
                NULL::varchar(50)                    AS place_of_service_source_value,
                0::integer                           AS place_of_service_concept_id
        FROM
                pharmacy_facilities__unioned pf
        
                JOIN  location l 
                    ON pf.finess8 = l.location_source_value)
                    
                    
SELECT * FROM pharmacy_hospital_location 
                
                
    