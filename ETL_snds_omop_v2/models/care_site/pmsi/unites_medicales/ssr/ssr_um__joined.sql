{{ config(materialized='view') }}

-- Ce modèle joint la table T_SSRaaB avec la liste des UM du SSR

WITH 
    ssr_finess AS(
        SELECT *
        FROM {{ ref('finess__unioned') }}
        WHERE pmsi_product = 'ssr'
    ),
        
    join_list_um AS(
        SELECT
            ssr.*,
            um.type_autorisation 

        FROM ssr_finess ssr

        LEFT JOIN {{ source('correspondance', 'correspondance_um_uf_ssr') }} um 
            ON ssr.type_um = um.type_um_pmsi
        
    )
    
SELECT * FROM join_list_um