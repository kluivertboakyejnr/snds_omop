{{config(materialized='table', indexes=[
      {'columns': ['num_enq', 'med_mtf_cod', 'imb_etm_nat'], 'unique': True}
    ])}}
-- Ce modèle corrige les dates de début et de fin d'ALD. Le diagnostic d'ALD étant renouvelé régulièrement, la date de début correspond à la date de début du diagnostic posé pour la première fois et la date de fin correspond à la date de fin du diagnostic le plus récent. 

WITH 
fix_ir_imb_r_dates AS
(
    SELECT 
        num_enq,
        med_mtf_cod, -- Motif médical ou pathologie (code CIM10)
        imb_etm_nat, -- Motif d'exonération du bénéficiaire
    
        MIN(imb_ald_dtd) AS imb_ald_dtd, -- Date de début du diagnstic posé pour la première fois
        MAX(imb_ald_dtf) AS imb_ald_dtf, -- Date de fin du diagnostic le plus récent
        MAX(ins_dte) AS ins_dte, -- Date d'insertion la plus récente
        MAX(upd_dte) AS upd_dte -- Date de MAJ la plus récente
    FROM {{ref('stg__ir_imb_r')}} 
    GROUP BY 1, 2, 3
)

SELECT * FROM fix_ir_imb_r_dates
   
    
    