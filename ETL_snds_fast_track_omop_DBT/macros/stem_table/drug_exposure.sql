{% macro drug_cases() %}

-- Cette macro calcule le nombre de doses (pilules, gouttes, etc.) administrées à une personne. Elle calcule les quantiés dans la table ER_PHA_F du DCIR et utilise la nomenclature IR_PHA_R

CASE
    WHEN pha_unt_nbr_dses IS NULL -- Certaines doses de médicaments sont pas dans IR_PHA_R
        THEN SUM(a.pha_act_qsn) -- jOn garde le nombre de boites
    WHEN (POSITION('+' IN pha_unt_nbr_dses) > 0) -- Certains médicaments sont sous la forme 3 pilules + 5 sachets : il est impossible de savoir lequel garder 
        THEN SUM(a.pha_act_qsn) -- on garde juste le nombre de boites
    WHEN 
        (POSITION('/' IN pha_unt_nbr_dses) = 0) -- doses de la forme "3 DOSES" ou "3", ce qui signifie que la boite contient 3 doses (solides ou liquides) et 3 doses solides.
        THEN
            (
            CASE 
            WHEN (POSITION(' ' IN pha_unt_nbr_dses) > 0) -- Cas pour "3 doses"
                THEN CAST(SUBSTR(pha_unt_nbr_dses, 0, POSITION(' ' IN pha_unt_nbr_dses)) AS INTEGER) * SUM(a.pha_act_qsn) -- On suppose que la quantité vaut nombre de boites * 3 
            ELSE -- Cas pour "3"  
                pha_unt_nbr_dses::integer * SUM(a.pha_act_qsn) -- La quantité est le nombre de boîtes * 3
            END
                )
    ELSE -- Lots de la forme  3 / 2 ML (la boîte contient 3 doses de 2 ML) 
        CAST(SUBSTR(pha_unt_nbr_dses, 0, POSITION('/' IN pha_unt_nbr_dses)) AS INTEGER) * SUM(a.pha_act_qsn) -- on suppose que la quantité est nombre de boîtes * 3 
END               quantity
{% endmacro %}




{% macro drug_pmsi(src_table, src_base, src_finess_nb, src_visit_nb, quantity = 'adm_nbr', dat_delai = 'dat_delai') %}

-- Cette macro contient du code répétitif qui ne peut être réduit. Il requête les différentes tables du PMSI qui alimentent la table OMOP DRUG_EXPOSURE
-- Cette macro fait appel à la macro visit_source_value
-- src_base est un champ du PMSI (MCO, HAD, SSr, RIP)
-- src_table est la table qui contient les informations
-- quantity est la variable qui contient le nombre de doses dans la src_table, si la colonne n'existe pas, on indique quantité = 1, par défaut, la variable est adm_nbr
-- delai est le nombre de jours entre le début de la visite et la prise du médicament, si cette colonne n'existe pas, on indique delai = 0, par défaut, la variable est dat_delai
-- src_finess_nb est la variable qui contient le numéro finess juridique d'un centre de soin dans la table src_table
-- src_visit est l'identifiant d'une visite dans la table src_table

SELECT 
    {{ quantity }}                       AS quantity,
    ucd_ucd_cod::text                    AS source_value,
    COALESCE({{ dat_delai }}, 0)         AS delai,
    
    {{ visit_source_value( 
        src_base      = src_base, 
        src_finess_nb = src_finess_nb, 
        src_visit_nb  = src_visit_nb) }} AS   visit_source_value
    
FROM 
    {{ src_table }}
                    
{% endmacro %}