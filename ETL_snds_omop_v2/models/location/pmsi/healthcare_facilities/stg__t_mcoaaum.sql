{{ config(materialized='view') }}


-- Ce modèle sélectionne les numéros finess juridiques et géographiques des établissements du MCO contenus dans les tables t_mcoaaUM

WITH union_mco_um_years AS(

    {% for year in var('years_pmsi') %}
    SELECT DISTINCT
        eta_num::varchar(50)                      AS finess_j, 
        eta_num_geo::varchar(50)                  AS finess_geo,
        substring(aut_typ1_um from '[^ ]+'::text) AS type_um, --Suprression de l'espace entre le nombre et le chiffre dans le code
        'mco'                                     AS pmsi_product

    FROM {{ source('MCO', 't_mco' ~ year ~ 'um') }}um

    {% if not loop.last -%}
    UNION ALL 
    {%- endif -%}

    {%- endfor -%})


SELECT * FROM union_mco_um_years

