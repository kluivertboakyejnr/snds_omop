{{ config(materialized='table') }}

WITH 
days_exposed_end_dates__joined AS
(
    SELECT 
        d.person_id, 
        d.ingredient_concept_id, 
        d.drug_sub_exposure_start_date, 
        e.end_date, 
        drug_exposure_count, 
        days_exposed::integer
    FROM {{ref('5_days_exposed__computed')}} d
    JOIN {{ref('6_end_dates__padded')}} e ON d.person_id = e.person_id AND d.ingredient_concept_id = e.ingredient_concept_id AND e.end_date >= d.drug_sub_exposure_start_date
),

cteDrugEraEnds  AS
(
SELECT
    person_id, 
    ingredient_concept_id, 
    drug_sub_exposure_start_date, 
    drug_exposure_count, 
    days_exposed::integer,
    
    MIN(end_date) AS era_end_date
    
FROM days_exposed_end_dates__joined
GROUP BY
        person_id, ingredient_concept_id, drug_sub_exposure_start_date, drug_exposure_count, days_exposed
)

SELECT * FROM cteDrugEraEnds