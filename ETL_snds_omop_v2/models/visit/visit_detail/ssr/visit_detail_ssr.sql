{{ config(materialized='view') }}


WITH 

    visit_detail_detail_ssr_b_c AS(
        SELECT
            jp_week,
            rhs_ant_sej_ent,
            visit_detail_nb,
            visit_detail_source_value,
            --admitting_source_value,
            --discharge_to_source_value,
            exe_soi_dtd,
            {{ visit_occurrence_source_value(src_base = "'ssr'",
                                            src_finess_nb = 'finess_j',
                                            src_visit_nb = 'visit_nb',
                                            src_start_date = 'exe_soi_dtd')}} AS visit_occurrence_source_value
    
        FROM {{ ref('ssr_c_b__joined') }} ),
        
                
    visit_occurrence_pmsi AS(
        SELECT
            *
        FROM {{ ref('visit_occurrence_pmsi') }}),
        
    care_site AS (
        SELECT 
            care_site_source_value,
            care_site_id
        FROM {{ ref('care_site') }}),
        
        
    ssr_b_visit__join AS(
        SELECT 
            v.person_id,
            v.visit_occurrence_id,
        
            c.care_site_id,
            
            CASE 
                WHEN s.visit_detail_nb = '1'
                THEN v.visit_start_date 
        
                ELSE v.visit_start_date + POSITION('1' IN jp_week) + rhs_ant_sej_ent - 1
        
            END visit_detail_start_date,
        
        
            CASE
                WHEN s.visit_detail_nb = '1'
                THEN v.visit_start_date + 8 - POSITION('1' IN REVERSE(jp_week)) - POSITION('1' IN jp_week)
                                                             
                ELSE v.visit_start_date + 8 - POSITION('1' IN REVERSE(jp_week)) + rhs_ant_sej_ent - 1
        
            END visit_detail_end_date,
        
            s.visit_detail_source_value,
            --s.admitting_source_value,
            --s.discharge_to_source_value,
        
            {{ visit_detail_id_source_value(src_base = "'ssr'",
                                            src_visit_occurrence_id = 'visit_occurrence_id',
                                            src_visit_detail_nb = 'visit_detail_nb') }} AS visit_detail_id_source_value
        
        FROM visit_detail_detail_ssr_b_c s
        
        JOIN visit_occurrence_pmsi v USING(visit_occurrence_source_value) 
    
        LEFT JOIN care_site c ON s.visit_detail_source_value = c.care_site_source_value)
        
        
SELECT * FROM ssr_b_visit__join
        
        
        
        
        
            
            
        
            
        