{{ config(materialized='view') }}

-- Ce modèle reconstitue le visit_source_value correspondant à la visite pendant laquelle l'acte a été réalisé, à partir des variables de er_prs_f. 
-- Cela permettra de joindre ces actes à la table VISIT_OCCURRENCE.

WITH 
dcir_procedure 
AS 
(
    SELECT 
    cam_prs_ide   AS source_value,
    quantity,
    {{
        visit_source_value(
            src_base="'dcir19_20'", 
            src_finess_nb='etb_pre_fin', 
            src_visit_nb='dcir_visit_id' 
        )
        }}           
        AS visit_source_value
FROM 
    {{ref('ccam_quantity__grouped')}} 

)

SELECT * FROM dcir_procedure