{{ config(materialized='view') }}

-- Ce modèle sélectionne les numéros finess juridiques et géographiques des établissements du champs SSR contenus dans les tables t_SSRaaB

SELECT DISTINCT
    eta_num::varchar(50)     AS finess_j, 
    eta_num_geo::varchar(50) AS finess_geo
    
FROM snds.t_ssr19_09b
