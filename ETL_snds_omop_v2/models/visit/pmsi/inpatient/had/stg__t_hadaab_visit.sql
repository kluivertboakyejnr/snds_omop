{{ config(materialized='view') }}

WITH 
    -- Sélectionner les identifiants d'une visite (finess_j, et visit_nb) et finess_geo 
    union_had_b_years AS(

    {% for year in var('years_pmsi') %}
        {% set source_name = 'HAD' %}
        {% set table_name = 't_had' ~ year ~ 'b'%}
        {% set base = ("'" ~ 'had' ~ year ~"'") | string %}
        
    SELECT DISTINCT
        eta_num_epmsi::varchar(50) AS finess_j, 
        rhad_num::varchar(50)      AS visit_nb,
        ent_prv                    AS ent_prv,
        sor_des                    AS sor_des,
        {{base}}                   AS base,
    
        {% if year|int < 17 %}
        eta_num_two::varchar(50)   AS finess_geo
    
        {% else %}    
        eta_num_geo::varchar(50)   AS finess_geo
    
        {% endif %}
    
        
        
    FROM {{ source(source_name, table_name) }}

    {% if not loop.last -%}
    UNION ALL 
    {%- endif -%}

    {%- endfor -%})


SELECT * FROM union_had_b_years