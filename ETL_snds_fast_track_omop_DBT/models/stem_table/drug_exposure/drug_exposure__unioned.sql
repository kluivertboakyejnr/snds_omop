{{ config(materialized='table', schema='stem', alias = 'stem_table_drug',  indexes = [{'columns' : var('stem_table_key'), 'unique' : True} ]) }}

-- Ce modèle unit les parties de la table DRUG_EXPOSURE relatives au PMSI, et au DCIR et sélectionne les variables utiles. 

WITH 
    all_drugs AS (
        {{ dbt_utils.union_relations(
            relations=[
                ref('drug_exposure_pmsi'),
                ref('drug_exposure_dcir_outpatient'),
                ref('drug_exposure_dcir_inpatient') ] ) }} )

SELECT
    DISTINCT
    domain_id::varchar(20)              AS domain_id,             --  Temporary assignement      
    person_id::bigint                   AS person_id,
    visit_occurrence_id::bigint         AS visit_occurrence_id,
    NULL::bigint                        AS visit_detail_id,
    provider_id::bigint                 AS provider_id,
    concept_id::bigint                  AS concept_id,          -- CIP13 concept to complete 
    source_value::varchar(50)           AS source_value,
    source_concept_id::bigint           AS source_concept_id,   -- CIP13 code, voir fichier concept 
    32810                               AS type_concept_id,     -- updated using the new standard types
    start_date::date                    AS start_date,
    (start_date||' 00:00:00')::timestamp AS start_datetime,
    end_date::date                      AS end_date,
    (end_date||' 00:00:00')::timestamp  AS end_datetime,
    NULL::date                          AS verbatim_end_date,
    days_supply::bigint                AS days_supply,
    dose_unit_source_value::varchar(50) AS dose_unit_source_value,
    NULL::varchar(50)                   AS lot_number,
    0::integer                          AS modifier_concept_id,
    NULL                                AS modifier_source_value,       -- If any additional information
    0                                   AS operator_concept_id,
    quantity::bigint                    AS quantity,            
    NULL                                AS range_high,
    NULL                                AS range_low,
    NULL::integer                       AS refills,
    0                                   AS route_concept_id,
    NULL                                AS route_source_value,
    NULL                                AS sig,
    NULL::varchar(20)                   AS stop_reason,
    NULL                                AS unique_device_id,
    0                                   AS unit_concept_id,
    NULL                                AS unit_source_value,
    0                                   AS value_as_concept_id,
    NULL                                AS value_as_number,
    NULL                                AS value_as_string,
    NULL                                AS value_source_value,
    0                                   AS anatomic_site_concept_id,
    0                                   AS disease_status_concept_id,
    NULL                                AS specimen_source_id,
    NULL                                AS anatomic_site_source_value,
    0                                   AS status_concept_id,
    NULL                                AS status_source_value,
    0                                   AS qualifier_concept_id,
    NULL                                AS qualifier_source_value
    
FROM 
    all_drugs

