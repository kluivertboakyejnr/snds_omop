{{ config(materialized='view') }}

-- Ce modèle joint le modèle dcir_device__selected (dispositifs médicaux) au modèle lpp_concept (correspondances entre les codes lpp et les codes SNOMED) et au modèle visit_occurrence_dcir (visites issues du SNDS) et sélectionne les variables pertienentes de la table DEVICE_EXPOSURE

WITH 
    dcir_device_selected AS (
        SELECT *
        FROM {{ ref('dcir_device__selected') }} ),        

    lpp_concept AS (
        SELECT *
        FROM {{ ref('lpp_concepts') }} ),

    visit_occurrence_dcir AS (
        SELECT *
        FROM {{ ref('visit_occurrence_dcir') }}),


    device_visit_concept__joined AS (   
        SELECT
            v.person_id::bigint                               AS person_id,
            v.visit_occurrence_id::bigint                     AS visit_occurrence_id,
            v.provider_id::bigint                             AS provider_id,
            dd.start_date::date                               AS start_date,
            COALESCE(dd.end_date, v.visit_end_date)::date     AS end_date,  
        
            dd.source_value::text                             AS source_value,
            dd.quantity::float                                AS quantity,  
        
            COALESCE(lc.source_concept_id,0)::integer         AS source_concept_id, -- voir dans fichier des concepts 
            COALESCE(lc.target_concept_id,0)::integer         AS concept_id,
            COALESCE(lc.domain_id, 'Device')::varchar(20)     AS domain_id
        
        FROM 
            dcir_device_selected dd
        
            JOIN visit_occurrence_dcir v USING(visit_source_value)
            
            LEFT JOIN lpp_concept lc USING(source_value))



SELECT * FROM device_visit_concept__joined

