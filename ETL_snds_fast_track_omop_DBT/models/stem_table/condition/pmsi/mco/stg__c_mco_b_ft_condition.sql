{{ config(materialized='view') }}

-- Ce modèle sélectionne les diagnostics principaux dans les tables B du MCO-FT

WITH diag_c_mco_b_ft_visit
AS
(
{{
    condition_source_values(
        src_table = 'snds.c_mco_b_ft', 
        source_values = ['dgn_pal'], 
        src_base = "'mcoft'", 
        src_finess_nb = 'eta_num', 
        src_visit_nb = 'rsa_num' 
    )
}}
)
SELECT * FROM diag_c_mco_b_ft_visit