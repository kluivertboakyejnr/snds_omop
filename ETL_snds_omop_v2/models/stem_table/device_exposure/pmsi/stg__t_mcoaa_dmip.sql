{{ config(materialized='view') }}

WITH device_mco_dmip AS (

    {% for year in var('years_pmsi') %}

        {% set source_name = 'MCO' %}
        {% set table_name = 't_mco' ~ year ~ 'dmip'%}
        {% set base = ("'" ~ 'mco' ~ year ~ "'") | string %}
        
        {{ device_exposure_pmsi(
            source_name   = source_name,
            source_value  = 'lpp_cod', 
            quantity      = 'nbr_pos', 
            delai         = 'delai', 
            src_base      = base, 
            src_finess_nb = 'eta_num', 
            src_visit_nb  = 'rsa_num', 
            src_table     = table_name ) }}


        {% if not loop.last -%}
        UNION ALL 
        {%- endif -%}

    {%- endfor -%}   
)

SELECT * FROM device_mco_dmip