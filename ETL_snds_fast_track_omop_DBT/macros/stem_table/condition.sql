{% macro condition_source_values(src_table, source_values, src_base, src_finess_nb, src_visit_nb) %}

-- Cette macro sélectionne une liste de variables (source_values) de la table src_table qui contiennent des diagnostics
-- src_base est un produit du PMSI (MCO, HAD, SSR, RIP)
-- src_table est la table dans laquelle on cherche les diagnostics
-- src_finess_nb est la variable qui contient le numéro finess juridique des établissements où sont posés les diagnostics dans la table src_table
-- src_visit_nb est l'identifiant d'une visite dans l'établissement associé au src_finess_nb

SELECT 
    DISTINCT 
    {% for sv in source_values %}
    {{sv}}            ,
    {%- endfor %}
    
    {{src_base}}      AS src_base,
    {{src_finess_nb}} AS src_finess_nb,
    {{src_visit_nb}}  AS src_visit_nb
FROM {{src_table}}
    
{% endmacro %}

{% macro condition_visit_status(src_value, src_table, status, status_concept_id) %}

-- Cette macro sélectionne des variables d'intéret pour la partie CONDITION de la STEM_TABLE et construit la variable visit_source_value associée aux visites dans lesquelles ont été posés les diagnotics pour qu'ils soient reliés par la suite à la table VISIT_OCCURRENCE.
-- Cette macro fait appel à la macro visit_source_value
-- source_value est la variable qui contient les diagnostics de la table src_table
-- status_source_value indique si le diagnostic est principal, relié ou associé dans le SNDS
-- status_concept_id indique si le diagnostic est primaire (principal) ou secondaire (relié ou associé)

SELECT 
    DISTINCT 
    {{ src_value }} AS source_value,
    {{ status }}    AS status_source_value, 
    {{ status_concept_id }} AS status_concept_id,
    {{visit_source_value('src_base', 'src_finess_nb', 'src_visit_nb')}} AS visit_source_value 
FROM 
    {{src_table}}
    
{% endmacro %}

