{{ config(materialized='view') }}

-- Ce modèle sélectionne les diagnostics principaux présents dans les tables t_hadaaB

WITH diag_t_hadaab_visit
AS
(
{{ condition_source_values(
        src_table = 'snds.t_had19_09b',
        source_values = ['dgn_pal'],
        src_base="'had19_09'",
        src_finess_nb='eta_num_epmsi',
        src_visit_nb='rhad_num'
)
}}
)


SELECT * FROM diag_t_hadaab_visit