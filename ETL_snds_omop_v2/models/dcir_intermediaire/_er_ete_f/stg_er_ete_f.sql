{{config(materialized='view')}}
-- Ce model sert à sélectionner les variables d'intérêt de la table ER_ETE_F et à faire l'union entre toutes les années disponibles.
-- Il sert également à supprimer les ACE des hôpitaux publics. 

WITH 
    union_er_ete_f_years AS (
        {% for year in var('years') %}
       
        SELECT
            prs_ppu_sec::integer,
            etb_exe_fin::varchar(100),
            ete_ind_taa::integer,
            ete_cat_cod::integer,
            {{ set_id(var('dcir_key')) }} AS dcir_key_id
        
        FROM {{ source('ER_ETE_F', 'er_ete_f_' ~  year  ) }}
        
        WHERE ete_ind_taa != 'ete_ind_taa'

            {% if not loop.last -%}
            UNION ALL 
            {%- endif -%}

        {%- endfor -%})


SELECT
    prs_ppu_sec,
    etb_exe_fin,
    ete_cat_cod, 
    dcir_key_id
FROM union_er_ete_f_years
WHERE ete_ind_taa != 1 -- Suppresion des ACE des hôpitaux publics 
AND dcir_key_id IN (SELECT dcir_key_id FROM {{ ref('bse_er_prs_f') }})
