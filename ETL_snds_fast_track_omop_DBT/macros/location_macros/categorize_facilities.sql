{% macro categorize_facilities() %}

-- Cette macro filtre les établissements du DCIR dont les données sont également présentes dans les champs du PMSI, 
-- à partir de leur catégorie disponible dans la nomenclature IR_CET_V. 

WHERE ( (etb_cat_rg1::integer > 1203) AND (ete_cat_cod::integer != 114) ) 
OR ( ete_cat_cod::integer IN (362, 422) )
   
{% endmacro %}
    
    
    