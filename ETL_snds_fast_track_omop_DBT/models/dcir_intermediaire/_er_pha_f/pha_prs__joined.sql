
{{config(materialized='table', indexes=[
      {'columns': ['dcir_key_id'], 'type': 'hash'}
    ])}}

-- Ce model permet de joindre er_pha_f et er_prs_f et de récupérer les variables d'intérêt 

WITH 
pha_prs_joined
AS
(
    SELECT
        a.pha_prs_c13,
        a.pha_act_qsn,
        b.prs_act_qte,
        b.psp_spe_cod,
        b.psp_act_nat,
        b.pse_spe_cod,
        b.etb_pre_fin,
        coalesce(b.pre_pre_dtd, b.exe_soi_dtd) AS pre_pre_dtd, -- on récupère la date de prescription, et à défaut, la date de dispensation en pharmacie 
        b.pfs_exe_num,

        b.dcir_key_id,
        b.dcir_visit_id
    FROM 
        {{ref('stg__er_pha_f')}} a
        JOIN {{ ref('bse__er_prs_f')}} b USING(dcir_key_id) 
)


SELECT * FROM pha_prs_joined 