{{ config(materialized='view') }}

WITH drug_exposure_had_medatu AS(

        {% for year in var('years_pmsi') %}

                {% set source_name = 'HAD' %}
                {% set table_name = 't_had' ~ year ~ 'medatu'%}
                {% set base = ("'" ~ 'had' ~ year ~ "'") | string %}

                {{ drug_pmsi(
                        src_source    = source_name,
                        src_table     = table_name , 
                        src_base      = base, 
                        src_finess_nb = 'eta_num_epmsi', 
                        src_visit_nb  = 'rhad_num') }}


                {% if not loop.last -%}
                UNION ALL 
                {%- endif -%}

        {%- endfor -%} )


SELECT * FROM drug_exposure_had_medatu