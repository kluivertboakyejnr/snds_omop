{{ config(materialized='table') }}

SELECT
        ROW_NUMBER() OVER (ORDER BY a.person_id)                                 AS observation_period_id,
        a.person_id::bigint                                                      AS person_id,
        GREATEST(MIN(c.start_date)::date, {{var('min_observation_start_date')}}) AS observation_period_start_date,
        CASE
                WHEN b.death_date IS NULL 
                THEN COALESCE(max(c.end_date), {{var('max_observation_end_date')}})  
                ELSE b.death_date::date
        END observation_period_end_date,
        32810 AS period_type_concept_id

FROM {{ ref('person') }} a
JOIN {{ref('computes_visit_stem_dates')}} c USING(person_id)
LEFT JOIN {{ ref('death') }}  b USING(person_id)

GROUP BY person_id, b.death_date


