{{ config(materialized='table', schema='omop') }}

SELECT 
    p.person_id::bigint       AS person_id,

    ki.ben_dcd_dte::date      AS death_date,
    ki.ben_dcd_dte::timestamp AS death_datetime,

    32810::integer            AS death_type_concept_id,


    --stc.source_concept_id::integer AS cause_concept_id,
    c.concept_id_2              AS cause_concept_id,
    c.concept_id_1              AS cause_source_concept_id,
    ki.dcd_cim_cod::varchar(50) AS cause_source_value


FROM {{ ref('person') }} p

JOIN {{ ref('ki_cci_r_ir_ben_r__joined') }}  ki ON p.person_source_value = ki.num_enq
LEFT JOIN  {{ref('athena_mapping')}} c ON ki.dcd_cim_cod = REPLACE(c.concept_code, '.', '')

WHERE c.vocabulary_id = 'CIM10'