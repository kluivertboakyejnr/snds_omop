{{ config(materialized='view') }}


WITH 
    union_years_rip_rsa AS (

        {% for year in var('years_pmsi') %}
        
            {% set source_name = 'RIP' %}
            {% set table_name = 't_rip' ~ year ~ 'rsa'%}
            {% set base = ("'" ~ 'rip' ~ year ~ "'") | string %}

            {{ condition_source_values(
                src_source    = source_name,
                src_table     = table_name,
                source_values = ['dgn_pal'],
                src_base      = base,
                src_finess_nb = 'eta_num_epmsi',
                src_visit_nb  = 'rip_num',
                src_sej_idt   = 'sej_idt') }}


            {% if not loop.last -%}
            UNION ALL 
            {%- endif -%}

        {%- endfor -%}      ),
        
        
    rip_c AS (
        SELECT
            base,
            finess_j AS finess_nb,
            visit_nb,
            exe_soi_dtd
        FROM {{ ref('stg__t_ripaac_visit') }}),
        
        
    rip_c_b AS (
        SELECT 
            rsa.*,
            c.exe_soi_dtd
    
        FROM union_years_rip_rsa rsa
        JOIN rip_c c USING(base, finess_nb, visit_nb))


SELECT * FROM rip_c_b