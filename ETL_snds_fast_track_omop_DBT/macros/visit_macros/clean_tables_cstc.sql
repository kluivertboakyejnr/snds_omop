{% macro clean_table_cstc( table_cstc, base, finess_j, visit_nb ) %}

-- Cette macro contient du code répété qui ne peut pas être réduit. Il requête les différentes tables du PMSI qui aliment la table OMOP VISIT_OCCURRENCE où visit_concept_id = 9202 (Outpatient)
-- base correspond au champ d'intêret du PMSI (MCO, HAD, SSR, RIP)
-- table_cstc est la table où sont les informations
-- finess_j est la variable qui contient le finess juridique dans la table_c
-- visit_nb est la variable qui contient l'identifiant unique d'une visite dans la table_cstc

SELECT
    c.num_enq::varchar(100)                                                       AS num_enq,
    {{ format_date('exe_soi_dtd') }}                                              AS exe_soi_dtd,
    COALESCE({{ format_date('exe_soi_dtf') }}, {{ format_date('exe_soi_dtd')}}  ) AS exe_soi_dtf,
    {{ base }}                                                                    AS base,
    c.{{ finess_j }}::varchar(50)                                                 AS finess_j,
    c.{{ visit_nb }}::varchar(50)                                                 AS visit_nb,
    9202                                                                          AS visit_concept_id


    
FROM {{ table_cstc }} c 

-- Dans cette macro, on sélectionne seulement les visites valides, c'est à dire pour lesquelles les codes retours valent 0
WHERE c.nir_ret = '0' 
    AND c.nai_ret = '0' 
    AND c.sex_ret = '0' 
    AND c.ias_ret = '0' 
    AND c.ent_dat_ret = '0'

{% endmacro %}