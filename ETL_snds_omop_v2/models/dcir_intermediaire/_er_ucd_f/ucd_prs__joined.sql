{{config(materialized='table')}}

WITH 
ucd_prs_joined 
AS
(
    SELECT
        a.ucd_ucd_cod,
        a.ucd_dlv_nbr*sign(b.prs_act_qte) AS ucd_dlv_nbr,
    
        b.psp_spe_cod,
        b.psp_act_nat,
        b.pse_spe_cod,
        b.etb_pre_fin,
        b.pre_pre_dtd,
        b.exe_soi_dtd,
        b.dcir_visit_id
    FROM 
        {{ref('stg_er_ucd_f')}} a
        JOIN {{ ref('bse_er_prs_f')}} b USING(dcir_key_id) 
)


SELECT * FROM ucd_prs_joined 