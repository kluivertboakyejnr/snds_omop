{{ config(materialized='view') }}

-- This models uses clean_table_c macro 
-- Removes non-valid lines 
-- Fixes dates format
-- Selects relevant variables to compute visit_source_value 

WITH union_rip_rsa_years AS(

    {%- for year in var('years_pmsi') -%}

        {% set source_name = 'RIP' %}
        {% set table_name = 't_rip' ~ year ~ 'rsa'%}
        {% set base = ("'" ~ 'rip' ~ year ~"'") | string %}

        SELECT 
             eta_num_epmsi AS finess_j,
            rip_num       AS visit_nb,
            sej_idt,
            ent_prv,
            sor_des,
            {{base}} AS base,
            eta_num_two

        FROM 
            {{ source(source_name, table_name) }} rsa

        {% if not loop.last -%}
        UNION ALL 
        {%- endif -%}

    {%- endfor -%})


SELECT * FROM union_rip_rsa_years