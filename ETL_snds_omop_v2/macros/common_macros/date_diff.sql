{% macro date_diff(start_date, end_date) %}

-- Cette macro calcule la différence entre deux dates.
-- date1 - date2 = x jours. La fonction DATE_PART garde uniquement l'entier x.  

DATE_PART('day', {{end_date}}::timestamp - {{start_date}}::timestamp)

{% endmacro %}