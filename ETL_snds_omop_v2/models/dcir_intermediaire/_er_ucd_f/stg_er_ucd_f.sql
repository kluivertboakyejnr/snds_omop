{{config(materialized='view')}}

WITH 
union_er_bio_f_years
AS
(
    {% for year in var('years') %}
    SELECT
        ucd_ucd_cod::varchar(50),
        ucd_dlv_nbr::integer,
        {{ set_id(var('dcir_key')) }} AS dcir_key_id
    FROM {{ source('ER_UCD_F', 'er_ucd_f_' ~  year  ) }}

        {% if not loop.last -%}
        UNION ALL 
        {%- endif -%}

    {%- endfor -%}
    
)


SELECT * FROM union_er_bio_f_years 
WHERE dcir_key_id IN (SELECT dcir_key_id FROM {{ ref('bse_er_prs_f') }})