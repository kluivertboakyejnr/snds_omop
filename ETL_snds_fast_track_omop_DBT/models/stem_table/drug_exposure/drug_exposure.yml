version: 2


# └── device_exposure
#     ├── pmsi
#     │   ├── had
#     │   │   ├── stg__t_hadaamed_drug.sql
#     │   │   ├── stg__t_hadaamedatu_drug.sql
#     │   │   └── stg__t_hadaamedchl_drug.sql
#     │   ├── mco
#     │   │   ├── stg__t_mcoaamed_drug.sql
#     │   │   ├── stg__t_mcoaamedatu_drug.sql
#     │   │   ├── stg__t_mcoaafhstc_drug.sql
#     │   │   └── stg__t_mcoaamedthrombo_drug.sql
#     │   ├── ssr
#     │   │   ├── stg__t_ssraamed_drug.sql
#     │   │   └── stg__t_ssraamedatu_drug.sql
#     │   └── drug_exposure_pmsi.sql
#     ├── dcir
#     │   ├── inpatient
#     │   └── outpatient
#     ├── lpp_concepts.sql
#     └── device_exposure.sql

models:
### DCIR ###
  - name: dcir_inpatient_drugs__selected
    description: Ce modèle sélectionne les variables de la table ER_UCD_F qui alimentent la table DRUG_EXPOSURE, il calcule provider_source_value et visit_source_value qui concernent la visite pendant laquelle le médicament a été délivré. 

  - name: drug_exposure_dcir_inpatient
    description: Ce modèle joint le modèle dcir_inpatient_drugs__selected avec les tables VISIt_OCCURRENCE et PROVIDER et avec le modèle ucd7_cip7 qui permet d'obtenir le concept standard RxNorm à partir des codes UCD7.

  - name: dcir_outpatient_drugs__selected
    description: Ce modèle selectionne les variables de la table ER_PHA_F qui alimentent la table DRUG_EXPOSURE, il calcule provider_source_value et visit_source_value qui concernent la visite pendant laquelle le médicament a été délivré. 

  - name: drug_exposure_dcir_outpatient
    description: Ce modèle joint le modèle dcir_outpatient_drugs__selected avec les tables VISIT_OCCURRENCE et PROVIDER et avec le modèle cip13_cip7 qui permet d'obtenir le concept standard RxNorm à partir des codes CIP-13.

### PMSI ###
### HAD ###
  - name: stg__t_hadaamed_drug
    description: Ce modèle sélectionne les variables de la table T_HADaaMED qui alimentent la table DRUG_EXPOSURE et construit le visit_source_value de la visite pendant laquelle a eu lieu la prise de médicament.

  - name: stg__t_hadaamedatu_drug
    description: Ce modèle sélectionne les variables de la table T_HADaaMEDATU qui alimentent la table DRUG_EXPOSURE et construit le visit_source_value de la visite pendant laquelle a eu lieu la prise de médicament.

  - name: stg__t_hadaamedchl_drug
    description: Ce modèle sélectionne les variables de la table T_HADaaMEDCHL qui alimentent la table DRUG_EXPOSURE et construit le visit_source_value de la visite pendant laquelle a eu lieu la prise de médicament.

### MCO ###
  - name: stg__t_mcoaafhstc_drug
    description: Ce modèle sélectionne les variables de la table T_MCOaaFHSTC qui alimentent la table DRUG_EXPOSURE et construit le visit_source_value de la visite pendant laquelle a eu lieu la prise de médicament.

  - name: stg__t_mcoaamed_drug
    description: Ce modèle sélectionne les variables de la table T_MCOaaMED qui alimentent la table DRUG_EXPOSURE et construit le visit_source_value de la visite pendant laquelle a eu lieu la prise de médicament.

  - name: stg__t_mcoaamedatu_drug
    description: Ce modèle sélectionne les variables de la table T_MCOaaMEDATU qui alimentent la table DRUG_EXPOSURE et construit le visit_source_value de la visite pendant laquelle a eu lieu la prise de médicament.

  - name: stg__t_mcoaamedthrombo_drug
    description: Ce modèle sélectionne les variables de la table T_MCOaaMEDTHROMBO qui alimentent la table DRUG_EXPOSURE et construit le visit_source_value de la visite pendant laquelle a eu lieu la prise de médicament.

### SSR ###
  - name: stg__t_ssraamed_drug
    description: Ce modèle sélectionne les variables de la table T_SSRaaMED qui alimentent la table DRUG_EXPOSURE et construit le visit_source_value de la visite pendant laquelle a eu lieu la prise de médicament.

  - name: stg__t_ssraamedatu_drug
    description: Ce modèle sélectionne les variables de la table T_SSRaaMEDATU qui alimentent la table DRUG_EXPOSURE et construit le visit_source_value de la visite pendant laquelle a eu lieu la prise de médicament.

  - name: drug_exposure_pmsi
    description: Ce modèle unit tous les modèles correspondant aux tables du PMSI qui alimentent la table DRUG_EXPOSURE, il les joint à la table VISIT_OCCURRENCE pour récupérer les identifiants du patient et de la visite et il joint également aux modèles ucd7_cip7 et ucd13_cip7 pour récupérer le code standard RxNorm correspondants aux codes ucd7 et ucd13. Il calcule la date de début de prise en fonction du délai et de la date de début de la visite, et le nombre de jours de prise et il somme les quantités sur tous les autres critères.

### COMMUN ###
  - name: ucd7_cip7
    description: Ce modèle joint la table des médicaments cip_ucd au modèle athena_mapping pour obtenir la correspondance entre un code UCD-7 et le code standard correspondant RxNorm.

  - name: ucd13_cip7
    description: Ce modèle joint la table des médicaments cip_ucd au modèle athena_mapping pour obtenir la correspondance entre un code UCD-13 et le code standard correspondant RxNorm.

  - name: cip13_cip7
    description: Ce modèle joint la table des médicaments cip_ucd au modèle athena_mapping pour obtenir la correspondance entre un code CIP-13 et le code standard correspondant RxNorm.

  - name: drug_exposure__unioned
    description: Ce modèle unit les parties de la table DRUG_EXPOSURE relatives au PMSI, et au DCIR et sélectionne les variables utiles.
    columns: 
      - name: person_id
        description: Identifiant unique d'une personne 
        tests:
          - not_null
          - relationships:
              to: ref('person')
              field: person_id

      - name: visit_occurrence_id
        description: Identifiant unique d'une visite 
        tests:
          - relationships:
              to: ref('visit_occurrence_id')
              field: visit_occurrence_id

      - name: provider_id
        description: Identifiant unique d'un professionnel de santé 
        tests:
          - relationships:
              to: ref('provider')
              field: provider_id

      - name: concept_id
        description: Concept standard RxNorm 
        tests:
          - not_null
          - relationships:
              to: ref('concept')
              field: concept_id

      - name: source_value
        description: Identifiant d'un médicament dans le SNDS (CIP-13, CIP-7, UCD-13, UCD-7)

      - name: source_concept_id
        description: Concept associé au code source



