{{ config(materialized='view') }}

-- Unit les finess juridiques et géographiques provenant des tables du PMSI et exclue les finess juridiques en double

WITH 
    finess_geo__unioned AS(
        {{ dbt_utils.union_relations(
            relations=[
                    ref('mco_e_um__joined'),
                    ref('had_e_b__joined'),
                    ref('ssr_e_b__joined'),
                    ref('rip_e_c__joined'),
                    ref('c_mco_e_um__joined')
            ] ) }} ),
            
    exclude_finess AS(
        SELECT * 
        FROM finess_geo__unioned fgu
    
        WHERE NOT EXISTS(
                SELECT *
                FROM snds.finess_exclusion_list fel
                WHERE fel.finess_to_exclude = fgu.finess_j)) -- Exclusion des FINESS géographiques APHP/APHM/HCL pour éviter les doublons (jusqu’en 2018) 
            
            
SELECT * FROM exclude_finess
