{{ config(materialized='table', schema='omop', indexes=[
      {'columns': ['person_id'], 'unique': True}
    ] ) }}

-- Ce modèle représente la table OMOP PERSON

WITH final_person AS
(
SELECT
        DISTINCT 
        {{set_id(['num_enq'])}}::bigint       AS person_id,
        CASE
                ben_sex_cod
                WHEN '1' THEN 8507
                WHEN '2' THEN 8532
        END                     AS gender_concept_id,
        ben_nai_ann::integer             AS year_of_birth,
        ben_nai_moi::integer             AS month_of_birth,
        NULL::integer                    AS day_of_birth,
        NULL::timestamp                  AS birth_datetime,
        num_enq::varchar(100)            AS person_source_value,
        location_id::bigint              AS location_id,
        0                                AS race_concept_id,
        0                                AS ethnicity_concept_id,
        NULL::bigint                     AS provider_id,
        0                                AS race_source_concept_id,
        0::varchar(50)                   AS race_source_value,
        0::varchar(50)                   AS ethnicity_source_value,
        0                                AS ethnicity_source_concept_id,
        ben_sex_cod::varchar(50)         AS gender_source_value,
        0                                AS gender_source_concept_id,
        NULL::bigint                     AS care_site_id
FROM
        {{ref('ir_ben_r_location__joined')}}
WHERE
        ben_sex_cod IN ('1','2')
)

SELECT * FROM final_person 