{{config(materialized='table')}}

WITH 
fix_ucd_quantity 
AS
(
    SELECT
        ucd_ucd_cod,
        psp_spe_cod,
        psp_act_nat,
        pse_spe_cod,
        etb_pre_fin,
        COALESCE(exe_soi_dtd, pre_pre_dtd) AS pre_pre_dtd, -- si la date de prescription n'est pas renseignée, prendre la date début de la visite
        exe_soi_dtd,
        dcir_visit_id,
    
        SUM(ucd_dlv_nbr) AS quantity

    FROM 
        {{ref('ucd_prs__joined')}}
    
    GROUP BY 1, 2,3,4,5,6,7,8
)


SELECT * FROM fix_ucd_quantity 