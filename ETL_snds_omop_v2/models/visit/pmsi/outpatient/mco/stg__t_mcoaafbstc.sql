{{ config(materialized='view') }}

WITH union_mco_fbstc_years AS(

    {%- for year in var('years_pmsi') -%}
        {% set source_name = 'MCO' %}
        {% set table_name = 't_mco' ~ year ~ 'fbstc'%}
        {% set base = ("'" ~ 'mco' ~ year ~ 'ace' ~ "'") | string %}
        {% if year | int > 15 %}
            SELECT 
                eta_num AS finess_j,
                seq_num AS visit_nb,
                eta_num_geo AS care_site_source_value,
                act_cod,
                exe_spe,
                {{base}} AS base
            FROM {{ source(source_name, table_name) }} fb
            WHERE SUBSTR(REVERSE(act_cod), 1, 2) NOT IN ('F ', 'N ')
            
            {% if not loop.last -%}
            UNION ALL 
            {%- endif -%}
        {% else %}
            SELECT 
                eta_num AS finess_j,
                seq_num AS visit_nb,
                eta_num AS care_site_source_value,
                act_cod,
                exe_spe,
                {{base}} AS base
            FROM {{ source(source_name, table_name) }} fb
            WHERE SUBSTR(REVERSE(act_cod), 1, 2) NOT IN ('F ', 'N ')
    
            {% if not loop.last -%}
            UNION ALL 
            {%- endif -%}
        {%-endif-%}
    {%- endfor -%}),

is_emergency AS 
(SELECT 
    finess_j, 
    visit_nb, 
    base,
    count(act_cod) FILTER (WHERE act_cod = 'ATU') AS emergency_count
FROM union_mco_fbstc_years fb
GROUP BY 1,2,3
),

emergency_lines AS (
SELECT 
    finess_j, 
    visit_nb, 
    base,
    exe_spe 
FROM union_mco_fbstc_years fb
WHERE act_cod = 'ATU'
),

select_exe_spe_emergencies AS
(
    SELECT 
        fb.finess_j, 
        fb.visit_nb, 
        fb.base,
        fb.care_site_source_value,
   
        em.emergency_count,
        CASE 
            WHEN emergency_count > 0 THEN el.exe_spe
            ELSE fb.exe_spe 
        END exe_spe
    
    FROM 
        union_mco_fbstc_years fb 
        JOIN is_emergency em USING(finess_j, visit_nb, base)
        LEFT JOIN emergency_lines el USING(finess_j, visit_nb, base)
),

count_multiple_exe_spe_per_visit AS (
    SELECT 
        ep.finess_j, 
        ep.visit_nb, 
        ep.base,
        ep.emergency_count,
    
        COUNT(DISTINCT exe_spe) as nb_exe_spe
    
    FROM 
        select_exe_spe_emergencies ep
    GROUP BY 1,2,3,4
),

select_exe_spe AS 
(
    SELECT 
        pe.finess_j, 
        pe.visit_nb, 
        pe.base,
        pe.care_site_source_value,
        pe.emergency_count,

        CASE 
            WHEN mp.nb_exe_spe > 1 THEN NULL
            ELSE pe.exe_spe
        END exe_spe
    FROM 
        select_exe_spe_emergencies pe 
        JOIN count_multiple_exe_spe_per_visit mp USING(finess_j, visit_nb, base)
)



SELECT 
    DISTINCT 
    * 
FROM select_exe_spe

/*,
                CASE 
                    WHEN SUBSTR(exe_spe, 1, 1) = '0' THEN SUBSTR(exe_spe, 2)
                    ELSE exe_spe
                END exe_spe,
                {{base}} AS base*/