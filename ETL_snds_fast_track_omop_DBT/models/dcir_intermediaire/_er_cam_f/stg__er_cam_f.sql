{{config(materialized='view')}}

-- Ce model sert à sélectionner les variables d'intérêt de la table ER_CAM_F et à faire l'union entre toutes les années disponibles.

WITH union_er_cam_f_years
AS
(
    SELECT
        cam_prs_ide::varchar(50), -- Code CCAM
        {{ set_id(var('dcir_key')) }} AS dcir_key_id
    FROM snds.er_cam_f_2019
    UNION
    SELECT
        cam_prs_ide::varchar(50),
        {{ set_id(var('dcir_key')) }} AS dcir_key_id
    FROM snds.er_cam_f_2020
)

SELECT 
    *
FROM 
    union_er_cam_f_years 


    