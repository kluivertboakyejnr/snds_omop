{{config(materialized='table')}}

WITH cam_join_prs
AS
(
    SELECT 
        a.cam_prs_ide,
    
        b.etb_pre_fin,
        b.prs_act_qte,
        b.num_enq,
        b.exe_soi_dtd,
        b.dcir_visit_id,
        b.cpl_maj_top
    FROM 
        {{ref('stg_er_cam_f')}} a
        JOIN {{ ref('bse_er_prs_f')}} b USING(dcir_key_id) 

)


SELECT 
    cam_prs_ide,
    
    etb_pre_fin,
    prs_act_qte,
    num_enq,
    exe_soi_dtd,
    dcir_visit_id
from cam_join_prs      
WHERE 
    cpl_maj_top < 2