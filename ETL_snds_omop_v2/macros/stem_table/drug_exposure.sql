{% macro drug_cases() %}

-- This macro computes the quantity of a doses (pills, drops, etc.) administrated to the person 
-- It is used to compute the quantities from the table ER_PHA_F in the DCIR, and needs the IR_PHA_R nomenclature of medicines 

CASE
    WHEN pha_upc_nbr IS NULL -- some medicines doses are not in IR_PHA_R
        THEN SUM(a.pha_act_qsn) -- just keep the number of boxes
    WHEN (POSITION('+' IN pha_upc_nbr) > 0) -- some medicines doses are of the form 3 pills + 5 bags : it is impossible to decide which one should be kept 
        THEN SUM(a.pha_act_qsn) -- just keep the number of boxes
    WHEN 
        (POSITION('/' IN pha_upc_nbr) = 0) -- doses of the form 3 DOSES  or 3, meaning the box contains 3 doses (solid or liquid) or 3 solid doses. 
        THEN
            (
            CASE 
            WHEN (POSITION(' ' IN pha_upc_nbr) > 0) -- case for 3 DOSES
                THEN CAST(SUBSTR(pha_upc_nbr, 0, POSITION(' ' IN pha_upc_nbr)) AS INTEGER) * SUM(a.pha_act_qsn) -- we assume the quantity is number of boxes * 3 
            ELSE -- case for 3  
                pha_upc_nbr::integer * SUM(a.pha_act_qsn) -- the quantity is number of boxes * 3 
            END
                )
    ELSE -- lots of doses are of the form 3 / 2 ML meaning the box contains 3 doses of 2 ML 
        CAST(SUBSTR(pha_upc_nbr, 0, POSITION('/' IN pha_upc_nbr)) AS INTEGER) * SUM(a.pha_act_qsn) -- we assume the quantity is number of boxes * 3 
END               quantity
{% endmacro %}




{% macro drug_pmsi(src_source, src_table, src_base, src_finess_nb, src_visit_nb, quantity = 'adm_nbr', dat_delai = 'dat_delai') %}

-- This macro contains repetitive code that cannot be reduced. It queries the different PMSI tables which will feed the OMOP-CDM DRUG_EXPOSURE table. 
-- This macro calls the visit_source_value macro 
-- src_base is the part of the PMSI (MCO, HAD, SSR, RIP) 
-- src_source id the source name
-- src_table is the table where info is stored 
-- quantity is the variable where the number of drugs (doses) is stored in the src_table, if the column does not exist, set quantity = 1, default is adm_nbr  
-- delai is the number of days that pass during the visit before drug administration, if the columns does not exist, set delai = 0, default is dat_delai  
-- src_finess_nb is the variable where the juridical identifier of a care site is stored in the src_table
-- src_visit_nb is the unique identifier of the visit where the condition was found in the src_table 

SELECT 
    {{ quantity }}::float                 AS quantity,
    ucd_ucd_cod::text                     AS source_value,
    COALESCE({{ dat_delai }}::integer, 0) AS delai,
    {{ src_base }}                        AS base, 
    {{ src_finess_nb }}                   AS finess_nb,
    {{ src_visit_nb }}                    AS visit_nb
        
FROM 
    {{ source(src_source, src_table) }}
                    
{% endmacro %}
