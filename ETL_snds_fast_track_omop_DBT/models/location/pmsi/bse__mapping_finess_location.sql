{{ config(materialized='table',
         indexes=[
      {'columns': ['ej_finess', 'et_finess'], 'unique': True}  ] ) }}

-- Ce modèle référence un fichier de correspondance qui permet de sélectionner des informations concernant l'adresse d'un établissement et sa raison sociale à partir de son numéro finess juridique ou géographique 
        

WITH 
    correct_code_commune AS(
        SELECT 
            ej_finess::varchar(50), 
            et_finess::varchar(50),
            finess8::varchar(50),
            rs::varchar(255),
            ej_rs::varchar(255),
            code_postal::varchar(9),
            {{ correct_code_commune(code = 'com_code') }}::varchar(9) AS com_code, -- Correction code commune Corse
            lib_acheminement::varchar(50),
            adresse::varchar(50)

        FROM snds.correspondance_finess_codecom3 )
        
        
SELECT
    ej_finess,
    et_finess,
    COALESCE(et_finess, ej_finess) AS finess_geo,
    finess8,
    rs,
    ej_rs,
    code_postal,
    com_code,
    {{ numero_departement(code_commune = 'com_code') }}::varchar(3) AS dpt_code, -- Correction code département Corse
    lib_acheminement,
    adresse
    
FROM correct_code_commune
    



        
