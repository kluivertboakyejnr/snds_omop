{{ config(materialized='view') }}

-- Jointure des finess géographiques avec les fichiers de correspondance pour récupérer des informations concernant l'adresse, la région sociale, le code département et le code région des établissements

WITH 
    mapping_finess_location AS(
        SELECT 
            finess_geo,
            adresse,
            lib_acheminement,
            com_code,
            dpt_code,
            rs,
            ej_rs
        
        FROM {{ ref('bse__mapping_finess_location') }} ),
        
            
    mapping_region_departement AS(
        SELECT 
            dpt_code,
            code_region
        FROM {{ ref('bse__mapping_region_departement') }} ),
        
        
        
    finess_geo AS(
        SELECT DISTINCT
            COALESCE(finess_geo, finess_j) AS finess_geo
        FROM {{ ref('finess__unioned')}}),
    
    
    finess_geo_mapped AS(
        SELECT DISTINCT
            maf.adresse,                             
            maf.lib_acheminement,
            maf.com_code,
            maf.rs AS care_site_name,
        
            mar.dpt_code,
            mar.code_region,

            fin.finess_geo

        FROM finess_geo fin
        
        LEFT JOIN mapping_finess_location maf USING(finess_geo)
            
        LEFT JOIN mapping_region_departement mar USING(dpt_code))
            
SELECT * FROM finess_geo_mapped