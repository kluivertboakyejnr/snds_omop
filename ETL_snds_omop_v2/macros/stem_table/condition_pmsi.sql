{% macro condition_source_values(src_source, src_table, source_values, src_base, src_finess_nb, src_visit_nb, src_sej_idt) %}
-- This macro selects a list of source values from PMSI tables where there are multiple source_values 
-- This macro calls the visit_source_value macro 
-- src_base is the part of the PMSI (MCO, HAD, SSR, RIP)
-- src_table is the table where info is stored 
-- source_value is the variable where the condition code is stored in the src_table 
-- src_finess_nb is the variable where the juridical identifier of a care site is stored in the src_table
-- src_visit_nb is the unique identifier of the visit where the condition was found in the src_table 

SELECT 
    DISTINCT 
    {% for sv in source_values %}
    {{sv}}            ,
    {%- endfor %}
    
    {{src_base}}      AS base,
    {{src_finess_nb}} AS finess_nb,
    {{src_visit_nb}}  AS visit_nb,
    {{src_sej_idt}}   AS sej_idt
    
FROM {{ source(src_source, src_table) }}
    
{% endmacro %}

{% macro condition_visit_status(src_value, src_table, status, status_concept_id, exe_soi_dtd=0) %}

SELECT 
    DISTINCT 
    {{ src_value }}         AS source_value,
    {{ status }}            AS status_source_value, 
    {{ status_concept_id }} AS status_concept_id,
    {{ visit_occurrence_source_value('base', 'finess_nb', 'sej_idt', exe_soi_dtd)}} AS visit_occurrence_source_value
    
FROM 
    {{src_table}}
    
{% endmacro %}

