{{ config(materialized='table') }}

WITH
minimum_start_date AS
(
    SELECT 
        person_id, 
        drug_concept_id, 
        drug_sub_exposure_end_date, 
        MIN(drug_exposure_start_date) AS drug_sub_exposure_start_date 
    FROM {{ref('3_drug_exposure_end_dates__minimized')}}
    GROUP BY person_id, drug_concept_id, drug_sub_exposure_end_date
),

cteSubExposures(row_number, person_id, drug_concept_id, drug_sub_exposure_start_date, drug_sub_exposure_end_date, drug_exposure_count) AS
(
    SELECT 
        ROW_NUMBER() OVER (
            PARTITION BY person_id, 
                drug_concept_id, 
                drug_sub_exposure_end_date 
            ORDER BY person_id
        ) AS row_number, 
        person_id, 
        drug_concept_id, 
        drug_sub_exposure_start_date, 
        drug_sub_exposure_end_date, 
        COUNT(*) AS drug_exposure_count
    FROM minimum_start_date
    GROUP BY person_id, drug_concept_id, drug_sub_exposure_start_date,drug_sub_exposure_end_date
)

SELECT * FROM cteSubExposures