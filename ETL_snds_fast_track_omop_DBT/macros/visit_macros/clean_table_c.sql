{% macro clean_table_c( table_c, base, finess_j, visit_nb ) %}

-- Cette macro contient du code répété qui ne peut pas être réduit. Il requête les différentes tables du PMSI qui aliment la table OMOP VISIT_OCCURRENCE où visit_concept_id = 9201 (Inpatient)
-- base correspond au champ d'intêret du PMSI (MCO, HAD, SSR, RIP)
-- table_c est la table où sont les informations
-- finess_j est la variable qui contient le finess juridique dans la table_c
-- visit_nb est la variable qui contient l'identifiant unique d'une visite dans la table_c


SELECT
    c.num_enq::varchar(100)           AS num_enq,
    {{ format_date('exe_soi_dtd') }} AS exe_soi_dtd,
    {{ format_date('COALESCE(exe_soi_dtf, exe_soi_dtd)') }} AS exe_soi_dtf,
    {{ base }}                       AS base,
    c.{{ finess_j }}::varchar(50)    AS finess_j,
    c.{{ visit_nb }}::varchar(50)    AS visit_nb,
    9201                             AS visit_concept_id


FROM
    {{ table_c }} c
    
-- Dans cette macro, on sélectionne seulement les visites valides, c'est à dire pour lesquelles les codes retours valent 0
WHERE 
    c.nir_ret = '0' 
    AND c.nai_ret = '0' 
    AND c.sex_ret = '0' 
    AND c.sej_ret = '0'
    AND c.fho_ret = '0' 
    AND c.pms_ret = '0' 
    AND c.dat_ret = '0'
    AND c.coh_nai_ret = '0' 
    AND c.coh_sex_ret = '0'
    
    
    
{% endmacro %}