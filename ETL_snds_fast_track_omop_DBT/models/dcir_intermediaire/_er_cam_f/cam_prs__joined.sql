{{config(materialized='table', indexes=[
      {'columns': ['dcir_key_id'], 'type': 'hash'}
    ])}}
-- Ce model sert à joindre la table ER_CAM_F à ER_PRS_F et à récupérer les variables de ER_PRS_F qui seront utilisée dans les tables OMOP alimentées par ER_CAM_F. 
-- on retire également les actes de majoration

WITH cam_join_prs
AS
(
    SELECT 
        a.cam_prs_ide, -- Code CCAM 
        b.etb_pre_fin,
        b.pfs_exe_num,
        b.prs_act_qte,
        b.num_enq,
        b.exe_soi_dtd,
        b.dcir_key_id,
        b.dcir_visit_id,
        b.cpl_maj_top  
    FROM 
        {{ref('stg__er_cam_f')}} a
        JOIN {{ ref('bse__er_prs_f')}} b USING(dcir_key_id) 

)


SELECT 
    * 
from cam_join_prs      
WHERE 
    cpl_maj_top < 2 -- On retire les actes de majoration 