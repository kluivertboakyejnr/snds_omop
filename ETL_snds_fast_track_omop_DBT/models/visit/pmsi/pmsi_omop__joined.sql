{{ config(materialized='view') }}

-- Ce model joint les visites du pmsi aux tables PERSON et CARE_SITE pour récupérer les variables person_id et care_site_id respectivement.
WITH 
    
    pmsi AS(
        SELECT 
            person_source_value,
            care_site_source_value,
            visit_start_date,
            visit_end_date,
            visit_concept_id,
            visit_source_value
        
    FROM {{ ref('visit_pmsi_variables__selected') }}),
            
    person AS(
        SELECT 
            person_source_value,
            person_id
        FROM {{ ref('person') }} ),
        
        
    care_site AS(
        SELECT 
            care_site_source_value,
            care_site_id
        FROM {{ ref('care_site') }}),
        
        
    pmsi_person_care_site_joined AS(
        SELECT 
            pers.person_id,
        
            cs.care_site_id,
        
            pmsi.visit_start_date,
            pmsi.visit_end_date,
            pmsi.visit_concept_id,
            pmsi.visit_source_value
            
            
        FROM pmsi 
            JOIN person pers USING(person_source_value)
            JOIN care_site cs USING(care_site_source_value) )
                    
                    
                    
SELECT * FROM pmsi_person_care_site_joined