{{ config(materialized='table') }}

{%- set table_exist = namespace(value=TRUE) -%}

{%- for year in var('years_pmsi') -%}
        {%- if year | int > 19 -%}
            {%- set table_exist.value = TRUE -%}
        {%-endif-%}
    {%-endfor-%}

{%- if table_exist.value -%}
    WITH drug_exposure_had_medchl AS(

        {% for year in var('years_pmsi') %}
    
            {% if year | int > 16 %}

                {% set source_name = 'HAD' %}
                {% set table_name = 't_had' ~ year ~ 'medchl'%}
                {% set base = ("'" ~ 'had' ~ year ~ "'") | string %}

                {{ drug_pmsi(
                        src_source    = source_name,
                        src_table     = table_name , 
                        src_base      = base, 
                        src_finess_nb = 'eta_num_epmsi', 
                        src_visit_nb  = 'rhad_num') }}


                {% if not loop.last -%}
                UNION ALL 
                {%- endif -%}
     
            {% endif %}

        {%- endfor -%} )


    SELECT * FROM drug_exposure_had_medchl
    
    {% else %}
    
        SELECT
            0   AS quantity,
            '0' AS source_value,
            0   AS delai,
            '0' AS base, 
            '0' AS finess_nb,
            '0' AS visit_nb
    
    {% endif %}


