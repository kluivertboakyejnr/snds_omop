{{config(materialized='view')}}
-- Ce model sert à sélectionner les variables d'intérêt de la table ER_ETE_F et à faire l'union entre toutes les années disponibles.
-- Il sert également à supprimer les ACE des hôpitaux publics. 

WITH 
union_er_ete_f_years
AS
(
    SELECT
        prs_ppu_sec::integer,
        etb_exe_fin::varchar(100),
        ete_ind_taa::integer,
        ete_cat_cod::integer,
        {{ set_id(var('dcir_key')) }} AS dcir_key_id
    
    FROM snds.er_ete_f_2019
    UNION ALL 
    SELECT
        prs_ppu_sec::integer,
        etb_exe_fin::varchar(100),
        ete_ind_taa::integer,
        ete_cat_cod::integer, 
        {{ set_id(var('dcir_key')) }} AS dcir_key_id
    
    FROM snds.er_ete_f_2020
    )


SELECT
    prs_ppu_sec,
    etb_exe_fin,
    ete_cat_cod, 
    dcir_key_id
FROM union_er_ete_f_years
WHERE ete_ind_taa != 1 -- suppression des ACE des hôpitaux publics 