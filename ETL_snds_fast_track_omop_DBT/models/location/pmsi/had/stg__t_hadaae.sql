{{ config(materialized='view') }}

-- Ce modèle sélectionne les numéros finess juridiques des établissements du champ HAD contenus dans les tables t_HADaaE

SELECT DISTINCT
    eta_num::varchar(50) AS finess_j
    
FROM snds.t_had19_09e

