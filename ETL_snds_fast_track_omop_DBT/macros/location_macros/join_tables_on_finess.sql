{% macro join_tables_on_finess(table_e, table_2) %}

-- Cette macro selectionne les finess juridiques et les finess géographiques correspondants des établissements en
-- réalisant l'union d'une table E (table_e) et d'une autre table du même champs du PMSI (table_2)

-- table_e : table E d'un champ du PMSI. Cette table contient le numéro finesss juridique
-- table_2 : table qui contient le numéro finess géographique

SELECT 
    a.finess_j,
    COALESCE(b.finess_geo, a.finess_j) AS finess_geo

FROM {{ table_e }} a
    JOIN {{ table_2 }} b
        ON a.finess_j = b.finess_j       
                
      
{% endmacro %}