{{ config(materialized='view') }}



WITH 
    correct_code_commune AS(
        SELECT
            code_region::varchar(2),
            nom_region,
            {{ correct_code_commune(code = 'com_code') }}::varchar(9) AS com_code,
            nom_commune
    
        FROM {{ source('correspondance', 'correspondance_region_departement' ) }} ) 
        
        
SELECT
    code_region,
    nom_region,
    com_code,
    {{ numero_departement(code_commune = 'com_code') }}::varchar(3) AS dpt_code,
    nom_commune
    
FROM correct_code_commune



