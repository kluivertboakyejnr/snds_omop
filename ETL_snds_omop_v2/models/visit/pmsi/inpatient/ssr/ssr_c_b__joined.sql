{{ config(materialized='table') }}

-- Ce modèle joint les tables T_SSRaaC et T_SSRaaB

WITH

    join_c_b AS(
        SELECT DISTINCT
            c.num_enq,
            c.exe_soi_dtd,
            c.exe_soi_dtf,
            'ssr' AS base,
            c.finess_j,
            c.visit_nb,
        
            b.rhs_num     AS visit_detail_nb,      -- Utile pour visit_detail
            b.jp_week,                                    -- Utile pour visit_detail
            b.rhs_ant_sej_ent,                            -- Utile pour visit_detail
            b.aut_typ_um  AS visit_detail_source_value,   -- Utile pour visit_detail
            b.hos_typ_um  AS visit_source_value,
            b.ent_prv     AS admitting_source_value,
            b.sor_des     AS discharge_to_source_value,
        
            NULL          AS provider_source_value,
        
            CASE
                WHEN b.hos_typ_um = '1' THEN 9201 -- Cas Hospitalisation complmète ou de semaine
                WHEN b.hos_typ_um = '2' THEN 9202 -- Cas Hospitalisation partielle de jour
                WHEN b.hos_typ_um = '3' THEN 9201 -- Cas Hospitalisation partielle de nuit
                WHEN b.hos_typ_um = '4' THEN 9202 -- Cas Séances
            END visit_concept_id
        
        
        FROM {{ ref('stg__t_ssraac_visit') }} c
        JOIN {{ ref('stg__t_ssraab_visit') }} b using(finess_j, visit_nb, base)
        
            
    )
    

SELECT * FROM join_c_b