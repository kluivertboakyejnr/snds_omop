{{ config(materialized='view') }} 
 
-- Ce modèle sélectionne les variables de la table ER_UCD_F qui alimentent la table DRUG_EXPOSURE, il calcule provider_source_value et visit_source_value qui concernent la visite pendant laquelle le médicament a été délivré. 

SELECT 
    ucd_ucd_cod::text                                         AS source_value,
    {{ provider_source_value('psp_spe_cod', 'psp_act_nat') }} AS provider_source_value,
    pre_pre_dtd                                               AS start_date,
    (pre_pre_dtd)::date                                       AS end_date, -- La date de fin de traitement dans le DCIR n'est pas connue.   
    quantity                                                  AS quantity,
    
    {{ visit_source_value( 
        src_base      = "'dcir19_20'", 
        src_finess_nb = 'etb_pre_fin', 
        src_visit_nb  = 'dcir_visit_id') }} AS visit_source_value
    
FROM {{ ref('ucd_quantity__grouped') }} 