{{ config(materialized='view') }}

-- Ce modèle sélectionne les variables de la table T_MCOaaFPSTC qui alimenteront la table DEVICE_EXPOSURE et calcule le visit_source_value associé à la pose ou l'achat du dispositif médical

{{ device_exposure_pmsi(
    source_value  = 'lpp_cod', 
    quantity      = 'lpp_qua', 
    delai         = 'del_dat_ent', 
    src_base      = "'mco19_09ace'", 
    src_finess_nb = 'eta_num', 
    src_visit_nb  = 'seq_num', 
    src_table     = 'snds.t_mco19_09fpstc') }}