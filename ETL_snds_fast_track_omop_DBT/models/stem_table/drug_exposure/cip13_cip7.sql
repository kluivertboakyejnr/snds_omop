{{ config(materialized='view') }}

-- Ce modèle joint la table des médicaments cip13 au schéma athena_mapping pour obtenir la correspondance entre un code cIP13 et le code standard RxNorm
SELECT 
    cu.codecip13::text AS source_value, -- Code CIP-13

    ath.concept_code, -- Code CIP-7
    ath.concept_id_1  AS source_concept_id, -- Concept associé au code CIP-7 dans ATHENA
    ath.concept_id_2  AS concept_id, -- Concept standard RxNorm
    ath.domain_id -- Domaine du code standard RxNorm

FROM 
    snds.cip_ucd cu

LEFT JOIN {{ ref('athena_mapping') }} ath 
    ON TRIM(codecip::text) = concept_code     
    WHERE vocabulary_id = 'BDPM'
