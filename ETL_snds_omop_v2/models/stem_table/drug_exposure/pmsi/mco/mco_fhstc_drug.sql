{{ config(materialized='view') }}

WITH 
    drug_exposure_mco_fhstc AS(

    {% for year in var('years_pmsi') %}

        {% set source_name = 'MCO' %}
        {% set table_name = 't_mco' ~ year ~ 'fhstc'%}
        {% set base = ("'" ~ 'mco' ~ year ~ 'ace' ~ "'") | string %}

        {{ drug_pmsi(
            src_source    = source_name,
            src_table     = table_name , 
            src_base      = base, 
            src_finess_nb = 'eta_num', 
            src_visit_nb  = 'seq_num', 
            quantity      = 'qua', 
            dat_delai     = 0 ) }}


        {% if not loop.last -%}
        UNION ALL 
        {%- endif -%}

    {%- endfor -%} )


SELECT * FROM drug_exposure_mco_fhstc


