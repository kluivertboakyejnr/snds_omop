SELECT co.person_id,
    co.condition_concept_id,
    co.condition_start_date,
    COALESCE(co.condition_end_date, condition_start_date::date + 1) AS condition_end_date
FROM {{ref('condition_occurrence')}} co