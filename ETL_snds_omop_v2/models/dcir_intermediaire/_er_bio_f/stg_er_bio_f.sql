{{config(materialized='view')}}

-- Ce modèle sélectionne les variables de le la table ER_BIO_F qui alimenteront les tables OMOP et unit toutes les années

WITH union_er_bio_f_years
AS
(
    {% for year in var('years') %}
        SELECT
            bio_prs_ide::varchar(50),
            bio_act_qsn::integer, 
            {{ set_id(var('dcir_key')) }} AS dcir_key_id
        FROM {{ source('ER_BIO_F', 'er_bio_f_' ~  year  ) }}
    
        WHERE bio_act_qsn != 'bio_act_qsn'

        {% if not loop.last -%}
                    UNION ALL 
                    {%- endif -%}

    {%- endfor -%})


SELECT * FROM union_er_bio_f_years
WHERE dcir_key_id IN (SELECT dcir_key_id FROM {{ ref('bse_er_prs_f') }})


