{{ config(materialized='view') }}

SELECT 
    person_id::bigint,
    condition_concept_id::bigint,
    condition_start_date::date,
    COALESCE(condition_end_date, condition_start_date + INTERVAL '1 day')::date AS condition_end_date
FROM {{ ref('condition_occurrence') }}