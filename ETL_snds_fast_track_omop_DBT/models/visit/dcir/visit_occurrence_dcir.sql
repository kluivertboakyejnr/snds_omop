{{config(materialized='table', indexes = [{'columns' : ['visit_occurrence_id'], 'unique' : True} ])}}

-- Ce modèle joint le modèle dcir_omop_fk__mapped (visites provenant des tables er_ete_f et er_prs_f) avec le fichier de correspondance qui permet d'obtenir le code standard d'une prestation (variable prs_nat_ref). Il représente la partie de la table VISIT_OCCURRENCE concernant le DCIR. 

WITH 
visit_concept_mapping AS
(
    SELECT  
        dcir.visit_source_value,
        dcir.visit_start_date, 
        dcir.visit_end_date,
        dcir.person_id,
        dcir.care_site_id,
        dcir.provider_id,
    
        COALESCE(corr.mapped_omop, 9202) AS visit_concept_id 
    FROM 
        {{ref('dcir_omop_fk__mapped')}} dcir
        LEFT JOIN snds.correspondance_prs_nat_ref_omop corr 
            ON corr.prs_nat_ref::bigint = dcir.prs_nat_ref::bigint
        
),

dcir_visits AS 
(
    SELECT
        {{set_id(['visit_source_value'])}}::bigint      AS visit_occurrence_id,
        person_id::bigint                       AS person_id,
        visit_concept_id::integer               AS visit_concept_id,
        visit_start_date::date                  AS visit_start_date,
        (visit_start_date::text||' 00:00:00')::timestamp          AS visit_start_datetime,
        visit_end_date::date                    AS visit_end_date,
        (visit_end_date::text||' 00:00:00')::timestamp          AS visit_end_datetime,
        32810                                   AS visit_type_concept_id,
        care_site_id::bigint                    AS care_site_id,
        visit_source_value::text                AS visit_source_value,
        provider_id::bigint                     AS provider_id,
        0                                       AS visit_source_concept_id,
        0                                       AS admitting_source_concept_id,
        NULL::varchar(50)                       AS admitting_source_value,
        0                                       AS discharge_to_concept_id,
        NULL::varchar(50)                       AS discharge_to_source_value,
        NULL::bigint                            AS preceding_visit_occurrence_id
FROM
        visit_concept_mapping
)

SELECT * FROM dcir_visits