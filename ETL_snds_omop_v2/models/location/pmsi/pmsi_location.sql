{{ config(materialized='view') }}


WITH 

    finess_geo AS(
        SELECT DISTINCT
            adresse                             AS address_1,
            NULL                                AS address_2,
            lib_acheminement                    AS city,
            code_region                         AS state,
            com_code                            AS zip,
            SUBSTR(com_code, 1, 2)::varchar(20) AS county,
            finess_geo                          AS location_source_value 
            
        FROM {{ ref('finess_geo__mapped') }}
        
     ),   
     
     departements AS(
         SELECT
             libelle AS address_1,
              NULL    AS address_2,
              NULL    AS city,
              reg     AS state,
              NULL    AS zip,
              dep     AS county,
              {{ numero_departement('dep') }}     AS location_source_value
         
         FROM {{ source('departements', 'departements_2020') }}
         
    )

SELECT * FROM finess_geo
UNION ALL 
SELECT * FROM departements
    


