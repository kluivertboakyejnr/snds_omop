{{config(materialized='view')}}

-- Ce modèle sélectionne les variables de ER_UCD_F qui alimenteront les tables OMOP et unit les années disponibles.

WITH 
union_er_bio_f_years
AS
(
    SELECT
        ucd_ucd_cod::varchar(50), -- Code UCD-13 du médicament 
        ucd_dlv_nbr::integer, -- Nombre de doses 
        {{ set_id(var('dcir_key')) }} AS dcir_key_id
    FROM snds.er_ucd_f_2019
    UNION ALL
    SELECT
        ucd_ucd_cod::varchar(50),
        ucd_dlv_nbr::integer,
        {{ set_id(var('dcir_key')) }} AS dcir_key_id
    FROM snds.er_ucd_f_2020
)


SELECT * FROM union_er_bio_f_years 