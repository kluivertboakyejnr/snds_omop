{{ config(materialized='view') }}

--Ce modèle sélectionne les diagnostics associés des tables t_mcoaaD

WITH diag_t_mcoaad_visit AS
(
    {{
        condition_source_values(
        src_table = 'snds.t_mco19_09d', 
        source_values = ['ass_dgn'], 
        src_base = "'mco19_09'", 
        src_finess_nb = 'eta_num', 
        src_visit_nb = 'rsa_num' 
    )
    
    }}
)

SELECT * FROM diag_t_mcoaad_visit