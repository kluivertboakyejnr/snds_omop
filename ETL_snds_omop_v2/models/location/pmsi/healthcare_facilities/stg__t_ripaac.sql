{{ config(materialized='view') }}

-- Ce modèle sélectionne les numéros finess juridiques et géographiques des établissements du champs RIP contenus 
-- dans les tables t_ripaaC

{%- set table_exist = namespace(value=TRUE) -%}

{%- for year in var('years_pmsi') -%}
        {%- if year | int > 19 -%}
            {%- set table_exist.value = FALSE -%}
        {%-endif-%}
    {%-endfor-%}


{%- if table_exist.value -%}
WITH union_rip_c_years AS(

    {% for year in var('years_pmsi') %}
    
    {% if year | int < 20 %}
    
    SELECT DISTINCT
    eta_num_epmsi::varchar(50) AS finess_j, 
    eta_num_two::varchar(50)   AS finess_geo,
    'rip'                      AS pmsi_product
        
    FROM {{ source('RIP', 't_rip' ~ year ~ 'c') }}
    
    
    

    {% if not loop.last -%}
    UNION ALL 
    {%- endif -%}
    
    {% endif %}

    {%- endfor -%})


SELECT * FROM union_rip_c_years


{% else %}

    SELECT
        '0'   AS finess_j,
        '0' AS finess_geo,
        '0'   AS pmsi_product

{% endif %}
