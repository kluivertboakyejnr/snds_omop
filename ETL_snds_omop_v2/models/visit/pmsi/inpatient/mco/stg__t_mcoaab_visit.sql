{{ config(materialized='table') }}

-- This model selects useful variables for VISIT_OCCURRENCE table from MCO B 

WITH union_mco_b_years AS (
    
    {%- for year in var('years_pmsi') -%}
    
        {% set source_name = 'MCO' %}
        {% set table_name = 't_mco' ~ year ~ 'b'%}
        {% set base = ("'" ~ 'mco' ~ year ~ "'") | string %}
    
            SELECT 
                eta_num AS finess_j,
                rsa_num AS visit_nb,
                ent_prv,
                sor_des,
                {{base}} AS base
            FROM 
                {{ source(source_name, table_name) }} c
    {% if not loop.last -%}
        UNION ALL 
        {%- endif -%}

    {%- endfor -%}
        
)

SELECT * FROM union_mco_b_years
 
    