{{ config(materialized='view') }}

SELECT 
    pha_prs_c13::text                                        AS source_value,
   {{ provider_source_value('psp_spe_cod', 'psp_act_nat') }} AS provider_source_value,
    pre_pre_dtd                                              AS start_date,
    (pre_pre_dtd + 29 )::date                                AS end_date,
    quantity                                                 AS quantity,
    pha_dos_uni                                              AS dose_unit_source_value,
    
    {{ visit_occurrence_source_value( 
        src_base      = "'dcir'", 
        src_finess_nb = 'etb_pre_fin', 
        src_visit_nb  = 'dcir_visit_id') }} AS visit_occurrence_source_value
    
    FROM 
        {{ ref('pha_quantity__grouped') }} 