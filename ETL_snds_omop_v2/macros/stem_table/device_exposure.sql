{% macro device_exposure_pmsi(source_name, source_value, quantity, delai, src_base, src_finess_nb, src_visit_nb, src_table) %}

-- This macro contains repetitive code that cannot be reduced. It queries the different PMSI tables which will feed the OMOP-CDM DEVICE_EXPOSURE table. 
-- This macro calls the visit_source_value macro 
-- src_base is the part of the PMSI (MCO, HAD, SSR, RIP) 
-- src_table is the table where info is stored 
-- source_value is the variable where the device's code is stored in the src_table
-- quantity is the variable where the number of devices is stored in the src_table, if the column does not exist, set quantity = 1  
-- delai is the number of days that pass during the visit before device administration, if the columns does not exist, set delai = 0  
-- src_finess_nb is the variable where the juridical identifier of a care site is stored in the src_table
-- src_visit_nb is the unique identifier of the visit where the condition was found in the src_table 


SELECT
    {{ source_value }}::text            AS source_value, --ou lpp_cod  
    {{ delai }}::integer                AS delai,
    {{ quantity }}::integer             AS quantity,

    {{ visit_occurrence_source_value(
        src_base      = src_base, 
        src_finess_nb = src_finess_nb, 
        src_visit_nb  = src_visit_nb) }} AS visit_occurrence_source_value 

FROM {{ source(source_name, src_table) }}    

                          
{% endmacro %}