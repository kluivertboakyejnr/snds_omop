{% macro clean_table_cstc( source_name, table_cstc, base, finess_j, visit_nb, care_site_sv ) %}

-- This macro contains repetitive code that cannot be reduces. It queries the different PMSI tables which will feed the OMOP-CDM VISIT_OCCURRENCE table where visit_concept_id is 9202 ( Outpatient) 
-- base is the part of the PMSI (MCO, HAD, SSR, RIP)
-- table_c is the table where info is stored 
-- finess_j is the variable where the juridical identifier is stored in the table_c
-- visit_nb is the unique identifier of the visit in table_c

SELECT
    c.num_enq::varchar(100)                                                       AS num_enq,
    {{ format_date('exe_soi_dtd') }}                                              AS exe_soi_dtd,
    COALESCE({{ format_date('exe_soi_dtf') }}, {{ format_date('exe_soi_dtd')}}  ) AS exe_soi_dtf,
    {{ base }}                                                                    AS base,
    c.{{ finess_j }}::varchar(50)                                                 AS finess_j,
    c.{{ visit_nb }}::varchar(50)                                                 AS visit_nb

    
FROM {{ source(source_name, table_cstc) }} c 

-- In this macro, we only select the valid visits, that is to say the 'codes retours' set to 0 
WHERE c.nir_ret = '0' 
    AND c.nai_ret = '0' 
    AND c.sex_ret = '0' 
    AND c.ias_ret = '0' 
    AND c.ent_dat_ret = '0'

{% endmacro %}