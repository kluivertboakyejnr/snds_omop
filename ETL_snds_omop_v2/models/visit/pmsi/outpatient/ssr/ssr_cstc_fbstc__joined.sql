{{ config(materialized='view') }}

WITH join_cstc_fbstc AS (
    SELECT 
        cstc.num_enq,
        cstc.exe_soi_dtd,
        cstc.exe_soi_dtf,
        cstc.base,
        cstc.finess_j,
        cstc.visit_nb,
        
        NULL      AS visit_source_value,
        NULL      AS admitting_source_value,
        NULL      AS discharge_to_source_value,
    
        fbstc.exe_spe AS provider_source_value,
        
        9202 AS visit_concept_id 
        
    FROM 
        {{ref('stg__t_ssraacstc_visit')}} cstc
        JOIN {{ref('stg__t_ssraafbstc')}} fbstc
        USING(finess_j, visit_nb)
)

SELECT * FROM join_cstc_fbstc