{{ config(materialized='view') }}

-- Rajouter ici le care_site_id = location_id

WITH    
   
    union_medical_unit_finess_j_and_geo AS (
        {{ dbt_utils.union_relations(
            relations=[
                ref('finess_geo__mapped'),
                ref('finess_j__mapped'),
                ref('ssr_um_var__selected'),
                ref('mco_um_var__selected')
            ])}}),
        
    
    location AS (
        SELECT 
            location_id,
            location_source_value
        FROM {{ ref('location') }}),
        
   
    pmsi_care_site AS(
        SELECT DISTINCT
            care_site_source_value,
            care_site_name,
            place_of_service_source_value,
            
            location_id
        
        FROM union_medical_unit_finess_j_and_geo
        
        LEFT JOIN location USING(location_source_value))

SELECT * FROM pmsi_care_site 