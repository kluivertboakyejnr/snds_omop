{{ config(materialized='view') }}

-- Ce modèle utilise la macro clean_table_c 
-- Il retire les lignes non valides 
-- Adapte le format des dates 
-- Sélectionne les variables qui permettent de calculer visit_source_value 


{{ clean_table_c(table_c = 'snds.t_had19_09c',
                 base = "'had19_09'",
                 finess_j = 'eta_num_epmsi',
                 visit_nb = 'rhad_num') }}
                 




