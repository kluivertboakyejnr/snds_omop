{{ config(materialized='view') }}

-- This models uses clean_table_c macro 
-- Removes non-valid lines 
-- Fixes dates format
-- Selects relevant variables to compute visit_source_value 

WITH union_mco_cstc_years AS(

    {%- for year in var('years_pmsi') -%}
    
        {% set source_name = 'MCO' %}
        {% set table_name = 't_mco' ~ year ~ 'cstc'%}
        {% set base = ("'" ~ 'mco' ~ year ~ 'ace' ~ "'") | string %}

        {{ clean_table_cstc(source_name = source_name,
                 table_cstc = table_name,
                 base = base,
                 finess_j = 'eta_num',
                 visit_nb = 'seq_num') }}

        {% if not loop.last -%}
        UNION ALL 
        {%- endif -%}

    {%- endfor -%})


SELECT * FROM union_mco_cstc_years
                 
