{{ config(materialized='view') }}

-- Ce modèle sélectionne les numéros finess juridiques et géographiques des établissements du champs HAD contenus dans les tables t_HADaaB. 
-- Il sélectionne également rhad_num qui sera utilisé pour la table VISIT_OCCURRENCE

SELECT DISTINCT
    eta_num_epmsi::varchar(50) AS finess_j, 
    eta_num_geo::varchar(50)   AS finess_geo,
    rhad_num::varchar(50)      AS visit_nb
    
FROM snds.t_had19_09b
