{% macro filter_concepts(source_vocabs) %}

-- Cette macro sélectionne les correspondances codes sources / concepts standards de la table SOURCE_TO_CONCEPT_MAP et effectue une jointure avec la table CONCEPT pour obtenir le domaine correspondant au concept standard

-- source_vocabs     : Liste des vocabulaires sources à considérer ex : ['CSARR', 'CCAM'] /!\ s'il n'y a qu'un seul vocabulaire, le renseigner quand même sous forme de liste 
-- ex : ['NABM']

-- source_code       : Code source dans le SNDS
-- source_concept_id : Concept correspondant au code source
-- target_concept_id : Concept standard cible
-- domain_id         : Domaine correspondant au concept standard


SELECT 
    src.source_code::text AS source_value,
    src.source_concept_id,
    src.target_concept_id,
    
    c.domain_id
    
    FROM 
        snds_omop.source_to_concept_map src
        
    JOIN snds_omop.concept c
        ON concept_id = target_concept_id 
    
    WHERE source_vocabulary_id IN ( {% for sv in source_vocabs %} {{sv}} {% if not loop.last %} , {% endif %} {% endfor %} ) 
    
    
    
{% endmacro %}