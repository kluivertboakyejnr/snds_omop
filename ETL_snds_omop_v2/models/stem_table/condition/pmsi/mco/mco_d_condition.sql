{{ config(materialized='view') }}

WITH 
    union_years_condition_dgn_ass_mco AS (

        {% for year in var('years_pmsi') %}
        
            {% set source_name = 'MCO' %}
            {% set table_name = 't_mco' ~ year ~ 'd'%}
            {% set base = ("'" ~ 'mco' ~ year ~ "'") | string %}

            {{ condition_source_values(
                src_source = source_name,
                src_table = table_name,
                source_values = ['ass_dgn'], 
                src_base = base,
                src_finess_nb='eta_num',
                src_visit_nb='rsa_num',
                src_sej_idt='rsa_num') }}

            {% if not loop.last -%}
            UNION ALL 
            {%- endif -%}

        {%- endfor -%}      )
    


SELECT * FROM union_years_condition_dgn_ass_mco