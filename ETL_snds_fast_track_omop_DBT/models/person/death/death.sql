{{ config(materialized='table', schema='omop') }}

-- Ce modèle sélectionne la date de décès des patients dans la table IR_BEN_R et effectue une jointure avec la table PERSON pour obtenir le person_id correspondant. 

SELECT
    person_id::bigint                        AS person_id,
    ben_dcd_dte::date                        AS death_date,
    (ben_dcd_dte||' 00:00:00')::timestamp    AS death_date_time,
    32815                                    AS death_type_concept_id,
    0                                        AS cause_concept_id,
    NULL::varchar(50)                        AS cause_source_value,
    0                                        AS cause_source_concept_id
FROM
    {{ref('ir_ben_r__deduplicated')}} a
JOIN {{ ref('person') }} b ON a.num_enq = b.person_source_value
WHERE a.ben_dcd_dte > '1600-01-01' 