{{ config(materialized='view') }}


SELECT 
    bio_prs_ide                               AS source_value, -- Code NABM de l'acte de biologie
    {{ visit_occurrence_source_value(
        src_base      = "'dcir'", 
        src_finess_nb = 'etb_pre_fin', 
        src_visit_nb  = 'dcir_visit_id' ) }}  AS visit_occurrence_source_value,
        quantity                              AS quantity 
        
        FROM {{ ref('bio_quantity__grouped') }} 