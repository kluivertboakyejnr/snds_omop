-- Ce modèle sélectionne les variables des tables t_ssraaCCAM utiles pour calculer visit_source_value associé à la visite pendant laquelle un actes a été réalisé, ainsi que celles qui alimenteront la table PROCEDURE_OCCURRENCE 

WITH procedure_t_ssraaccam AS
(
    {{
    procedure_pmsi_source_value(
        src_table = 'snds.t_ssr19_09ccam', 
        source_value = 'ccam_act', 
        src_base = "'ssr'", 
        src_finess_nb = 'eta_num', 
        src_visit_nb = 'rha_num',
        phase = 'ccam_pha_act',
        delai = 'ccam_del_ent_um'
    )
}}
)

SELECT * FROM procedure_t_ssraaccam