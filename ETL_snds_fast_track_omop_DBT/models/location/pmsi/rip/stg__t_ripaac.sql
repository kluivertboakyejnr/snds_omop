{{ config(materialized='view') }}

-- Ce modèle sélectionne les numéros finess juridiques et géographiques des établissements du champs RIP contenus dans les tables t_ripaaC

SELECT DISTINCT
    eta_num_epmsi::varchar(50) AS finess_j, 
    eta_num_two::varchar(50)   AS finess_geo
    
FROM snds.t_rip19_09c
