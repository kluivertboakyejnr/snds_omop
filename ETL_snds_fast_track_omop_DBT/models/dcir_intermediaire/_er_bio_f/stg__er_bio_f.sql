{{config(materialized='view')}}

-- Ce modèle sélectionne les variables de le la table ER_BIO_F qui alimenteront les tables OMOP et unit toutes les années
    
WITH union_er_bio_f_years
AS
(
    SELECT
        bio_prs_ide::varchar(50), -- Code NABM de l'acte de biologie
        {{ set_id(var('dcir_key')) }} AS dcir_key_id
    FROM snds.er_bio_f_2019
    
    UNION ALL 
    SELECT
        bio_prs_ide::varchar(50),
        {{ set_id(var('dcir_key')) }} AS dcir_key_id
    FROM snds.er_bio_f_2020
)

SELECT * FROM union_er_bio_f_years


