{{ config(materialized='view') }}

-- Ce model joint les finess juridiques au fichier de correspondance pour récupérer les raisons sociales correspondantes

WITH 
    mapping_finess_location AS(
        SELECT 
            ej_finess AS finess_j,
            ej_rs
        
        FROM {{ ref('bse__mapping_finess_location') }} ),
        

    finess_j__unioned AS(
        SELECT DISTINCT
            finess_j
        FROM {{ ref('finess_j__unioned')}}),
    
    
    finess_j_mapped AS(
        SELECT DISTINCT
            maf.ej_rs,
        
            fin.finess_j
            
        FROM finess_j__unioned fin
        
        JOIN mapping_finess_location maf USING(finess_j) )           
           
            
SELECT * FROM finess_j_mapped